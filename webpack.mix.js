const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/assets/js')
  .sourceMaps();

mix.sass('resources/assets/sass/app.scss', 'public/assets/css')
  .version()
// speeding up, but this is for copying dependency images
// .options({ processCssUrls: false })
;

mix.copyDirectory('resources/assets/fonts', 'public/assets/fonts')
  .copyDirectory('resources/assets/images', 'public/assets/images')
  .copyDirectory('resources/assets/downloads', 'public/assets/downloads')
;

mix.browserSync('www.forestias.test');
