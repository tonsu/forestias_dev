<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('pages.index');
})->name('index');

Route::get('/about', function () {
  return view('pages.about');
})->name('about');

Route::get('/family', function () {
  return view('pages.family');
})->name('family');

Route::get('/facility', function () {
  return view('pages.facility');
})->name('facility');

Route::get('/contact', function () {
  return view('pages.contact');
})->name('contact');

Route::get('/blog', function () {
  return view('pages.blog');
})->name('blog');

Route::get('/single', function () {
  return view('pages.single');
})->name('single');

Route::get('/404', function () {
  return view('pages.404');
})->name('404');
