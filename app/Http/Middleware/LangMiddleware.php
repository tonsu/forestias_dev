<?php
/**
 * Frontend Dev Note:
 * This is a simple mechanism for language without /{lang}/ in the url.
 * This is the only middleware I created.
 */

namespace App\Http\Middleware;

use Closure;

class LangMiddleware {
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    if ($request->input('lang')) {
      app()->setLocale($request->input('lang'));
      $request->session()->put('lang', $request->input('lang'));
    } else if($request->session()->has('lang')) {
      app()->setLocale($request->session()->get('lang'));
    }
    return $next($request);
  }
}
