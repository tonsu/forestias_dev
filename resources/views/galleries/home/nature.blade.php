<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/home/nature/nature-1.jpg'),
		'caption'    => __('home.nature_caption'),
		'disclaimer'    => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
