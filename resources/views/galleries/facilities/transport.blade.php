<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/fac-transport.jpg'),
		'caption'    => __('facilities.transport_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
