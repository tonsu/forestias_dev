<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/medical/medical-1.jpg'),
		'caption'    => __('facilities.medical_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/fac/medical/medical-2.jpg'),
		'caption'    => __('facilities.medical_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
