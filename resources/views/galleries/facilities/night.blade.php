<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/night/night-1.jpg'),
		'caption'    => __('facilities.night_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/fac/night/night-2.jpg'),
		'caption'    => __('facilities.night_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
