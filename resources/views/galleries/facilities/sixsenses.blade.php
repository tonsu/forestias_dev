<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/sixsenses/sixsenses-1.jpg'),
		'caption'    => __('facilities.sixsenses_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/fac/sixsenses/sixsenses-2.jpg'),
		'caption'    => __('facilities.sixsenses_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
