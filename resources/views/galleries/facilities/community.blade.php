<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/community/community-1.jpg'),
		'caption'    => __('facilities.community_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/fac/community/community-2.jpg'),
		'caption'    => __('facilities.community_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
