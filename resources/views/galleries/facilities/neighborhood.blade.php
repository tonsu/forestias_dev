<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/neighborhood/neighborhood-1.jpg'),
		'caption'    => __('facilities.neighborhood_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/fac/neighborhood/neighborhood-2.jpg'),
		'caption'    => __('facilities.neighborhood_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
