<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/fac/shows/show-1.jpg'),
		'caption'    => __('facilities.shows_caption'),
		'disclaimer' => false,
		'content'    => __('facilities.shows_img_content')
	],
	// [
	// 	'thumbnail'  => asset('/assets/images/fac/shows/show-2.jpg'),
	// 	'caption'    => __('facilities.shows_caption'),
	// 	'disclaimer' => false,
	// 	'content'    => __('facilities.shows_img_content')
	// ],
	// [
	// 	'thumbnail'  => asset('/assets/images/fac/shows/show-3.jpg'),
	// 	'caption'    => __('facilities.shows_caption'),
	// 	'disclaimer' => false,
	// 	'content'    => __('facilities.shows_img_content')
	// ],
	[
		'thumbnail'  => asset('/assets/images/fac/shows/show-4.jpg'),
		'caption'    => __('facilities.shows_caption'),
		'disclaimer' => false,
		'content'    => __('facilities.shows_img_content')
	],
	// [
	// 	'thumbnail'  => asset('/assets/images/fac/shows/show-5.jpg'),
	// 	'caption'    => __('facilities.shows_caption'),
	// 	'disclaimer' => false,
	// 	'content'    => __('facilities.shows_img_content')
	// ],
	[
		'thumbnail'  => asset('/assets/images/fac/shows/show-6.jpg'),
		'caption'    => __('facilities.shows_caption'),
		'disclaimer' => false,
		'content'    => __('facilities.shows_img_content')
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide slide-centered gallery-container",
 ])
@endcomponent
