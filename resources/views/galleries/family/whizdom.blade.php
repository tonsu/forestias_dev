<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/project/whizdom/whizdom-1.jpg'),
		'caption'    => __('family.whizdom_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/whizdom/whizdom-2.jpg'),
		'caption'    => __('family.whizdom_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/whizdom/whizdom-3.jpg'),
		'caption'    => __('family.whizdom_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
