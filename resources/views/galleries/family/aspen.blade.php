<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/project/aspen/aspen-tree-1.jpg'),
		'caption'    => __('family.aspen_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/aspen/aspen-tree-2.jpg'),
		'caption'    => __('family.aspen_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/aspen/aspen-tree-3.jpg'),
		'caption'    => __('family.aspen_caption'),
		'disclaimer' => true,
	],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
