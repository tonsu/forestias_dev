<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/project/sixsenses/six-senses-residences-1.jpg'),
		'caption'    => __('family.sixsenses_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/sixsenses/six-senses-residences-2.jpg'),
		'caption'    => __('family.sixsenses_caption'),
		'disclaimer' => true,
	],
  [
	  'thumbnail'  => asset('/assets/images/project/sixsenses/six-senses-residences-3.jpg'),
	  'caption'    => __('family.sixsenses_caption'),
	  'disclaimer' => true,
  ],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
