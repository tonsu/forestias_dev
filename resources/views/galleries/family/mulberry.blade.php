<?php
$items = [
	[
		'thumbnail'  => asset('/assets/images/project/mulberry/mulberry-groove-1.jpg'),
		'caption'    => __('family.mulberry_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/mulberry/mulberry-groove-2.jpg'),
		'caption'    => __('family.mulberry_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/mulberry/mulberry-groove-5.jpg'),
		'caption'    => __('family.mulberry_caption'),
		'disclaimer' => true,
	],
	[
		'thumbnail'  => asset('/assets/images/project/mulberry/mulberry-groove-3.jpg'),
		'caption'    => __('family.mulberry_caption'),
		'disclaimer' => true,
	],
	// [
	// 	'thumbnail'  => asset('/assets/images/project/mulberry/mulberry-groove-4.jpg'),
	// 	'caption'    => __('family.mulberry_caption'),
	// 	'disclaimer' => true,
	// ],
] ?>
@component('partials.gallery', [
  'items' => $items,
  'container_classes' => "slide gallery-container mb-100vw",
 ])
@endcomponent
