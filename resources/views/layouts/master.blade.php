<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewproject-fit=cover">
  <title>@yield('title', 'THE FORESTIAS')</title>
  <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png') }}">
  <link rel="stylesheet" href="{{ mix('/assets/css/app.css') }}">
  @include('partials.inlinescript')
</head>
<?php
?>
<body
    @unless(empty($body_class))
    class="{{$body_class}}"
    @endunless
>
@include('partials.svgs')
@include('partials.preloader')
@include('partials.nav')
@include('partials.modals.register')
@include('partials.modals.video')
@include('partials.modals.fullwidth')
@include('partials.photoviewer')

<div id="scrollContainer">
  @yield('content')
  @include('partials.footer')
</div>

@include('partials.side_buttons')

<script type="text/javascript" src="{{ mix('/assets/js/app.js') }}"></script>

</body>
</html>
