<div id="family-hero" class="tac-vh-100 nav-padding">
  <div class="container">
    <div class="fh-content-wrap">
      <h5 class="color-3 text-center">
        {!! __('family.hero_title') !!}
      </h5>
      <div class="project-select">
        <div class="project-select-item" data-target="#project-whizdom">
          <img src="{{asset('/assets/images/project-whizdom.png')}}" alt="Whizdom"/>
        </div>
        <div class="project-select-item" data-target="#project-mulberry">
          <img src="{{asset('/assets/images/project-mulberry.png')}}" alt="Mulberry"/>
        </div>
        <div class="project-select-item" data-target="#project-aspen">
          <img src="{{asset('/assets/images/project-aspen-tree.png')}}" alt="Aspen Tree"/>
        </div>
        <div class="project-select-item" data-target="#project-sixsenses">
          <img src="{{asset('/assets/images/project-six-senses.png')}}" alt="Six Senses"/>
        </div>
      </div>
    </div>
  </div>
</div>
