<?php
$php_vars = [
	'text_image_disclaimer' => __('common.image_disclaimer'),
	'text_image_disclaimer_2' => __('common.image_disclaimer_2'),
	'text_register' => __('common.register'),
  'locale' => app()->getLocale(),
  'autoplayTimeout' => 6000,
  'register_modal_show_default' => __('common.register_modal_show_default'),
]
?>
<script type="text/javascript">

  var php_vars = <?php echo json_encode($php_vars); ?>;
  window.php_vars = php_vars

  // Preloading
  // ----------------------------------------

  window.onload = function () {
    document.body.className += ' loaded'
    document.body.className = document.body.className.replace('loading', '');
  }

  // Cross-browser Document Ready check
  var domIsReady = (function(domIsReady) {
    var isBrowserIeOrNot = function() {
      return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
    }

    domIsReady = function(callback) {
      if(callback && typeof callback === 'function'){
        if(isBrowserIeOrNot() !== 'ie') {
          document.addEventListener("DOMContentLoaded", function() {
            return callback();
          });
        } else {
          document.attachEvent("onreadystatechange", function() {
            if(document.readyState === "complete") {
              return callback();
            }
          });
        }
      } else {
        console.error('The callback is not a function!');
      }
    }

    return domIsReady;
  })(domIsReady || {});

  (function(document, window, domIsReady, undefined) {
    domIsReady(function() {
      document.body.className += ' dom-ready'
    });
  })(document, window, domIsReady)

</script>

<style>
  body, html {
    margin: 0;
    padding: 0;
    background-color: #ffffff;
  }

  #preloader {
    width: 100vw;
    height: 100%;
    overflow: hidden;
    position: fixed;
    background: #ffffff;
    z-index: 99;
    transition: all 1s;
  }

  #plWrap {
    width: 100%;
    height: 100%;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
  }


  #plWrap svg {
    width: 7.5vw;
  }

  body.dom-ready #plWrap {
    opacity: 1;
  }

  body:not(.loaded) {
    overflow: hidden;
  }

  #scrollContainer {
    visibility: hidden;
  }

  @media only screen and (max-width: 992px) {
    #plWrap svg {
      width: 40vw;
    }
  }

  .last-section:after {
    content: '<?php echo __('common.image_disclaimer_footer') ?>';
  }
</style>
