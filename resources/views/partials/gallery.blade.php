<?php
$the_container_classes = "";
if (isset($container_classes)) $the_container_classes .= $container_classes
?>
<div class="<?php echo $the_container_classes ?>">
	<?php
	foreach ($items as $item):
	$item['isSlide'] = true;
	?>

  <div class="slide-item">
  @component('partials.gallery_item', $item)
  @endcomponent

  <!-- Content is to be display below the image (not the same as caption, there can be both at the same time) -->
		<?php if (isset($item['content'])) : ?>
    <div class="slide-content">
			<?php echo $item['content'] ?>
    </div>
		<?php endif; ?>
  </div>

	<?php endforeach; ?>
</div>
