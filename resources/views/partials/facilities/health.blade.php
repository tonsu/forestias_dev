<div id="fac-health" class="theme-light last-section">
  <div class="container">
    <div class="fh-top section-effect">
      {!! __('facilities.health_title') !!}
    </div>
    <div class="fh-content-wrap theme-facility" itemscope itemtype="http://schema.org/ImageGallery">
      <div class="fh-item gallery-trigger">
        <div class="d-none gallery-container" id="galleryHealth1">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/everyday/everyday-1.jpg'),
            'caption' => __('facilities.health_1_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/everyday/everyday-2.jpg'),
            'caption' => __('facilities.health_1_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/everyday/everyday-3.jpg'),
            'caption' => __('facilities.health_1_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/everyday/everyday-4.jpg'),
            'caption' => __('facilities.health_1_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/everyday/everyday-5.jpg'),
            'caption' => __('facilities.health_1_caption')
          ])
          @endcomponent
        </div>
        <figure class="gallery-item forestias-img" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img class="w-100" src="{{asset('/assets/images/fac/health/health-1.jpg')}}"/>
          <div class="fhi-content-inside">
            <img class="fhi-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
            <h4>{!! __('facilities.health_1_subtitle') !!}</h4>
            <h2>{!! __('facilities.health_1_title') !!}</h2>
            <p>{!! __('facilities.health_1_content') !!}</p>
          </div>
        </figure>
        <div class="fhi-content section-effect">
          <h4>{!! __('facilities.health_1_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_1_title') !!}</h2>
        </div>
      </div>
      <div class="fh-item gallery-trigger">
        <div class="d-none gallery-container" id="galleryHealth2">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/intergenerational/intergenerational-1.jpg'),
            'caption' => __('facilities.health_2_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/intergenerational/intergenerational-2.jpg'),
            'caption' => __('facilities.health_2_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/intergenerational/intergenerational-3.jpg'),
            'caption' => __('facilities.health_2_caption')
          ])
          @endcomponent
        </div>
        <div class="fhi-content section-effect">
          <h4>{!! __('facilities.health_2_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_2_title') !!}</h2>
        </div>
        <figure class="gallery-item forestias-img" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img class="w-100" src="{{asset('/assets/images/fac/health/health-2.jpg')}}"/>
          <div class="fhi-content-inside">
            <img class="fhi-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
            <h4>{!! __('facilities.health_2_subtitle') !!}</h4>
            <h2>{!! __('facilities.health_2_title') !!}</h2>
            <p>{!! __('facilities.health_2_content') !!}</p>
          </div>
        </figure>
      </div>
      <div class="fh-item gallery-trigger">
        <div class="d-none gallery-container" id="galleryHealth3">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/education-discovery/education-discovery-1.jpg'),
            'caption' => __('facilities.health_3_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/education-discovery/education-discovery-2.jpg'),
            'caption' => __('facilities.health_3_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/education-discovery/education-discovery-3.jpg'),
            'caption' => __('facilities.health_3_caption')
          ])
          @endcomponent
        </div>
        <figure class="gallery-item forestias-img" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img class="w-100" src="{{asset('/assets/images/fac/health/health-3.jpg')}}"/>
          <div class="fhi-content-inside">
            <img class="fhi-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
            <h4>{!! __('facilities.health_3_subtitle') !!}</h4>
            <h2>{!! __('facilities.health_3_title') !!}</h2>
            <p>{!! __('facilities.health_3_content') !!}</p>
          </div>
        </figure>
        <div class="fhi-content section-effect">
          <h4>{!! __('facilities.health_3_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_3_title') !!}</h2>
        </div>
      </div>
      <div class="fh-item gallery-trigger">
        <div class="d-none gallery-container" id="galleryHealth4">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-1.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-2.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-3.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-4.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-5.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/fac/health/learn-together/learn-together-6.jpg'),
            'caption' => __('facilities.health_4_caption')
          ])
          @endcomponent
        </div>
        <div class="fhi-content section-effect">
          <h4>{!! __('facilities.health_4_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_4_title') !!}</h2>
        </div>
        <figure class="gallery-item forestias-img" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img class="w-100" src="{{asset('/assets/images/fac/health/health-4.jpg')}}"/>
          <div class="fhi-content-inside">
            <img class="fhi-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
            <h4>{!! __('facilities.health_4_subtitle') !!}</h4>
            <h2>{!! __('facilities.health_4_title') !!}</h2>
            <p>{!! __('facilities.health_4_content') !!}</p>
          </div>
        </figure>
      </div>
    </div>
  </div>

  <!-- Mobile -->
  <div class="health-content-mb d-lg-none">

    <div class="slide" data-opt-dots="true" data-opt-nav="false">
      <div class="slide-item">
        <figure class="forestias-img slide-figure" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img src="{{asset('/assets/images/fac/health/health-1.jpg')}}" itemprop="thumbnail" alt="Image description">
        </figure>
        <div class="slide-content">
          <h4>{!! __('facilities.health_1_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_1_title') !!}</h2>
          <p>{!! __('facilities.health_1_content') !!}</p>
        </div>
      </div>
      <div class="slide-item">
        <figure class="forestias-img slide-figure" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img src="{{asset('/assets/images/fac/health/health-2.jpg')}}" itemprop="thumbnail" alt="Image description">
        </figure>
        <div class="slide-content">
          <h4>{!! __('facilities.health_2_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_2_title') !!}</h2>
          <p>{!! __('facilities.health_2_content') !!}</p>
        </div>
      </div>
      <div class="slide-item">
        <figure class="forestias-img slide-figure" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img src="{{asset('/assets/images/fac/health/health-3.jpg')}}" itemprop="thumbnail" alt="Image description">
        </figure>
        <div class="slide-content">
          <h4>{!! __('facilities.health_3_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_3_title') !!}</h2>
          <p>{!! __('facilities.health_3_content') !!}</p>
        </div>
      </div>
      <div class="slide-item">
        <figure class="forestias-img slide-figure" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
          <img src="{{asset('/assets/images/fac/health/health-4.jpg')}}" itemprop="thumbnail" alt="Image description">
        </figure>
        <div class="slide-content">
          <h4>{!! __('facilities.health_4_subtitle') !!}</h4>
          <h2>{!! __('facilities.health_4_title') !!}</h2>
          <p>{!! __('facilities.health_4_content') !!}</p>
        </div>
      </div>
    </div>
  </div>

</div>
