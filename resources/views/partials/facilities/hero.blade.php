<section id="fac-hero" class="tac-vh-100 nav-padding with-notice-2">
  <div id="facHeroBannerCnt">
    <div id="facHeroBanner">
      <img src="{{asset('assets/images/fac/' . __('facilities.hero_image'))}}"/>
    </div>
  </div>
</section>
