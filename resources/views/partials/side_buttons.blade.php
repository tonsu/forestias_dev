<div id="sideButtons">
  <button id="btnChat" class="btn-plain btn-chat">
    <svg><use xlink:href="#tic-chat"></use></svg>
  </button>
  <button id="backToTop" class="btn-plain sync-right">
    <img src="{{asset('/assets/images/back-to-top.svg')}}"/>
  </button>
</div>
