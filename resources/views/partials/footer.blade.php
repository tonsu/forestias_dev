<div id="footer" class="footer">
  <div class="container">

    <div class="footer-inner">
      <div class="footer-top">
        <div class="logos-links-wrap">
          <div class="footer-logos">
            <a href="#"><img src="{{asset('/assets/images/logo-mqdc-white@2x.png')}}"/></a>
            <a href="#"><img src="{{asset('/assets/images/logo-forestias-white@2x.png')}}"/></a>
          </div>
          <ul class="footer-links">
            <li><a href="{{ route('index') }}" class="current">{!!__('nav.home')!!}</a></li>
            <li><a href="{{ route('about') }}">{!!__('nav.about')!!}</a></li>
            <li><a href="{{ route('facility') }}">{!!__('nav.facilities')!!}</a></li>
            <li><a href="{{ route('family') }}">{!!__('nav.family')!!}</a></li>
            <li><a href="{{ route('contact') }}">{!!__('nav.contact')!!}</a></li>
            <li><a href="{{ route('blog') }}">{!!__('nav.blog')!!}</a></li>
          </ul>
        </div>
        <div class="footer-subscribe">
          <h5>{!! __('nav.subscribe_title') !!}</h5>
          <div class="form-wrap">
            <input class="form-control" type="text" placeholder="{{__('nav.subscribe_placeholder')}}"/>
            <button class="btn btn-secondary">
              <span>{{__('nav.subscribe')}}</span>
            </button>
          </div>
        </div>
      </div>

      <div class="footer-bottom">
        <div class="color-secondary footer-copyright">{!! __('nav.copyright') !!}</div>
        <div class="socials-call-wrap">
          <div class="footer-socials">
            <a target="_blank" href="{{ __('links.facebook') }}"><img src="{{asset('assets/images/icon-facebook.svg')}}"/></a>
            <a target="_blank" href="{{ __('links.twitter') }}"><img src="{{asset('assets/images/icon-twitter.svg')}}"/></a>
            <a target="_blank" href="{{ __('links.youtube') }}"><img src="{{asset('assets/images/icon-youtube.svg')}}"/></a>
          </div>
          <a class="footer-call" href="tel:1265">
            <img src="{{asset('/assets/images/icon-mobile.svg')}}"/>
            <div class="call-content">
              <div class="call-text color-4">CALL CENTER</div>
              <div class="call-number">1265</div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </div>
</div>
