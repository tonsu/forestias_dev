<?php
$image = isset($image) ? $image : $thumbnail;
$data = getimagesize($image);
$width = $data[0];
$height = $data[1];
$classes = "gallery-item forestias-img";
if (isset($disclaimer) && $disclaimer == true) $classes .= " with-disclaimer";
if (isset($isSlide) && $isSlide == true) $classes .= " slide-figure";
?>
<figure class="<?php echo $classes ?>"
        itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
  <!-- Link to the large image -->
  <a href="<?php echo $image ?>" itemprop="contentUrl" data-size="<?php echo $width . 'x' . $height ?>">
    <!-- Thumbnail -->
    <div class="gallery-thumb-wrap">
      <img class="gallery-thumb" src="<?php echo $thumbnail ?>" itemprop="thumbnail"
           alt="Image description"/>
    </div>
  </a>
  <figcaption itemprop="caption description">
		<?php if(isset($caption)) echo $caption ?>
  </figcaption>
</figure>
