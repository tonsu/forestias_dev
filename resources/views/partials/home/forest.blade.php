<!-- Section: Forest -->
<section id="home-forest" class="theme-light last-section section-effect">
  <div class="container">
    <div class="text-center">
      {!! __('home.forest_title') !!}
      <div class="desc">
        {!! __('home.forest_content') !!}
      </div>
    </div>

    <!-- Mobile -->
    <div class="forest-map-mb d-lg-none with-notice notice-color-secondary notice-text-center">
      <img class="w-100" src="{{asset('/assets/images/home/' .  __('home.forest_map_mb'))}}"/>
    </div>

    <div class="forest-content-mb d-lg-none">
			<?php
			$items = [
				[
					'thumbnail'  => asset('/assets/images/home/forest/forest-1.jpg'),
					'caption'    => __('home.forest_1_caption'),
					'disclaimer' => true,
          'content' => '<h5 class="color-primary">'.__('home.forest_1_title').'</h5>'.
            __('home.forest_1_content'),
				],
				[
					'thumbnail'  => asset('/assets/images/home/forest/forest-2.jpg'),
					'caption'    => __('home.forest_2_caption'),
					'disclaimer' => true,
          'content' => '<h5 class="color-primary">'.__('home.forest_2_title').'</h5>'.
            __('home.forest_2_content'),
				],
				[
					'thumbnail'  => asset('/assets/images/home/forest/forest-3.jpg'),
					'caption'    => __('home.forest_3_caption'),
					'disclaimer' => true,
          'content' => '<h5 class="color-primary">'.__('home.forest_3_title').'</h5>'.
            __('home.forest_3_content'),
				],
				[
					'thumbnail'  => asset('/assets/images/home/forest/forest-4.jpg'),
					'caption'    => __('home.forest_4_caption'),
					'disclaimer' => true,
          'content' => '<h5 class="color-primary">'.__('home.forest_4_title').'</h5>'.
            __('home.forest_4_content'),
				],
			] ?>
      @component('partials.gallery', [
        'items' => $items,
        'container_classes' => "slide gallery-container mb-100vw has-dots no-nav",
       ])
      @endcomponent
    </div>

    <!-- Desktop -->
    <div class="forest-content d-none d-lg-block gallery-container">
      <div class="fc-row">
        <div class="fc-col">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/home/forest/forest-1.jpg'),
            'disclaimer' => true,
          ])
          @endcomponent
          <h5 class="color-primary">{!! __('home.forest_1_title') !!}</h5>
          {!! __('home.forest_1_content') !!}
        </div>
        <div class="fc-col">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/home/forest/forest-2.jpg'),
            'disclaimer' => true,
          ])
          @endcomponent
          <h5 class="color-primary">{!! __('home.forest_2_title') !!}</h5>
          {!! __('home.forest_2_content') !!}
        </div>
      </div>
      <div class="fc-row">
        <div class="fc-col">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/home/forest/forest-3.jpg'),
            'disclaimer' => true,
          ])
          @endcomponent
          <h5 class="color-primary">{!! __('home.forest_3_title') !!}</h5>
          {!! __('home.forest_3_content') !!}
        </div>
        <div class="fc-col">
          @component('partials.gallery_item', [
            'thumbnail' => asset('/assets/images/home/forest/forest-4.jpg'),
            'disclaimer' => true,
          ])
          @endcomponent
          <h5 class="color-primary">{!! __('home.forest_4_title') !!}</h5>
          {!! __('home.forest_4_content') !!}
        </div>
      </div>
      <div class="forest-map">
        <img class="w-100" src="{{asset('/assets/images/home/' .  __('home.forest_map'))}}"/>
      </div>
    </div>

    <div class="btn-view-trails text-uppercase color-3 d-none d-lg-block">
      {!! file_get_contents(asset('/assets/images/icon-view.svg')) !!}
      {!! __('home.view_walking_trails') !!}
    </div>

  </div>
</section>
