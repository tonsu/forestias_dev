<div id="walkingTrails">

  {{--  Desktop Only --}}
  <div class="container d-none d-lg-block">
    <div class="wt-content">
      <div class="wt-left bg-center-contain"
           style="background-image: url({{asset('assets/images/home/' . __('home.walking_trail_image'))}})"></div>
      <div class="wt-right">
        {!! __('home.walking_trail_title') !!}
        {!! __('home.walking_trail_content') !!}
        <div class="wt-legend">
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #CB0301"></div>
            {!! __('home.walking_trail_legend_red') !!}
          </div>
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #FB8505"></div>
            {!! __('home.walking_trail_legend_orange') !!}
          </div>
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #D9CBBE"></div>
            {!! __('home.walking_trail_legend_gray') !!}
          </div>
        </div>
      </div>
    </div>
  </div>

  {{--  Mobile Only --}}
  <div class="container d-lg-none">
    <div class="wt-content-mb">
      <div class="scroll-container">
        <img class="w-100" src="{{asset('assets/images/home/' . __('home.walking_trail_image_mb'))}}"/>
        <div class="wt-legend">
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #CB0301"></div>
            {!! __('home.walking_trail_legend_red') !!}
          </div>
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #FB8505"></div>
            {!! __('home.walking_trail_legend_orange') !!}
          </div>
          <div class="wt-legend-item">
            <div class="legend-color" style="background-color: #D9CBBE"></div>
            {!! __('home.walking_trail_legend_gray') !!}
          </div>
        </div>
        {!! __('home.walking_trail_title') !!}
        {!! __('home.walking_trail_content') !!}
      </div>
    </div>
  </div>

</div>
