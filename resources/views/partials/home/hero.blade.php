<section id="home-hero" class="tac-vh-100 nav-padding">
  <div id="homeHeroWrap" class="position-relative w-100 h-100">
    <div id="homeHeroLogo" class="hero-logo">
      <img src="{{asset('/assets/images/forestias-text.svg')}}" alt="logo"/>
      <p>{!! __('home.hero_subtitle') !!}</p>
    </div>
    <div class="slide with-notice no-nav" id="homeHeroSlide"
         data-opt-dots="true"
         data-opt-auto-height="false">
      <div class="slide-item" style="background-image: url({{asset('/assets/images/home/banner-1.jpg')}}"></div>
      <div class="slide-item" style="background-image: url({{asset('/assets/images/home/banner-2.jpg')}}"></div>
      <div class="slide-item" style="background-image: url({{asset('/assets/images/home/banner-3.jpg')}}"></div>
      <div class="slide-item" style="background-image: url({{asset('/assets/images/home/banner-4.jpg')}}"></div>
    </div>
  </div>
</section>

<div id="homeDeco">
  <div class="home-deco">
    <img class="leaf leaf-1-1" src="{{asset('/assets/images/home/leaf.png')}}"/>
    <img class="leaf leaf-1-2" src="{{asset('/assets/images/home/leaf.png')}}"/>
    <img class="leaf leaf-2-1" src="{{asset('/assets/images/home/leaf.png')}}"/>
    <img class="leaf leaf-2-2" src="{{asset('/assets/images/home/leaf.png')}}"/>
  </div>
</div>
