<div class="nav-container" id="stickyNav">
  <div class="container">
    <div class="nav-inner">

      <!-- Left Section -->
      <a href="{{ route('index') }}" class="nav-sec-left">
        <img class="nav-logo" src="{{asset('assets/images/logo.svg')}}"/>
      </a>

      <!-- Middle Section -->
      <div class="nav-sec-center">
        <div id="navDynamicContent"></div>
        @unless(empty($nav_title))
          <div class="nav-title color-primary">
            {{$nav_title}}
          </div>
        @endunless
      </div>

      <!-- Dummy, For mobile -->
      <div class="nav-sec-right-mb"></div>

      <!-- Right Section -->
      <div class="nav-sec-right">
        <button id="btnRegister" class="btn btn-primary" data-toggle="modal" data-target="#modalRegister">
          {!! __('common.register') !!}
        </button>
        <!--  Toggle-->
        <div id="menuToggle">
          <input id="menuCheckbox" type="checkbox"/>
          <div id="navBackdrop"></div>
          <div class="nav-content">
            <ul class="nav-langs">
              <li><a data-locale="th" href="?lang=th">TH</a></li>
              <li><a data-locale="en" href="?lang=en">EN</a></li>
              <li><a data-locale="cn" href="?lang=cn">中文</a></li>
            </ul>
            <ul class="nav-links">
              <li><a href="{{ route('index') }}">{!!__('nav.home')!!}</a></li>
              <li><a href="{{ route('about') }}">{!!__('nav.about')!!}</a></li>
              <li><a href="{{ route('facility') }}">{!!__('nav.facilities')!!}</a></li>
              <li><a href="{{ route('family') }}">{!!__('nav.family')!!}</a></li>
              <li><a href="{{ route('contact') }}">{!!__('nav.contact')!!}</a></li>
              <li><a href="{{ route('blog') }}">{!!__('nav.blog')!!}</a></li>
            </ul>
            <div class="nav-footer">
              <div class="d-flex justify-content-end">
                <a target="_blank" href="{!! __('common.download_brochure_link') !!}" class="btn btn-4 btn-brochure">
                  <img src="{{asset('assets/images/icon-download.svg')}}"/>
                  {!! __('common.download_brochure') !!}
                </a>
              </div>
              <div class="social-calls-wrap">
                <div class="nav-socials">
                  <a target="_blank" href="{{ __('links.facebook') }}"><img src="{{asset('assets/images/icon-facebook.svg')}}"/></a>
                  <a target="_blank" href="{{ __('links.twitter') }}"><img src="{{asset('assets/images/icon-twitter.svg')}}"/></a>
                  <a target="_blank" href="{{ __('links.youtube') }}"><img src="{{asset('assets/images/icon-youtube.svg')}}"/></a>
                </div>
                <a class="nav-call" href="tel:1265">
                  <img src="{{asset('assets/images/icon-mobile.svg')}}"/>
                  <div class="call-content">
                    <h3 class="color-4">CALL CENTER</h3>
                    <h4>1265</h4>
                  </div>
                </a>
              </div>
              <div class="nav-copyright">
                {!! __('nav.copyright') !!}
              </div>
            </div>
          </div>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
  </div>

</div>
