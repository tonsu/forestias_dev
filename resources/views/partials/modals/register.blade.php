<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      <span>{!! __('common.close') !!}</span>{!! file_get_contents(asset('/assets/images/icon-x.svg')) !!}
      </button>
    </div>
    <div class="modal-content">

      <form id="formRegister">
        <div class="modal-body text-center" style="padding-bottom: 0;">

          <h2 class="color-primary">{!! __('common.register') !!}</h2>

          <div class="form-group text-left">
            <select class="form-control mdb-select md-form">
              <option disabled selected hidden>{!! __('common.your_interest') !!}*</option>
              <option value="whizdom_condominiums">Whizdom Condominiums</option>
              <option value="mulberry_grove">Mulberry Grove</option>
              <option value="aspen_tree_residences">The Aspen Tree Residences</option>
              <option value="six_senses_residences">Six Senses Residences</option>
            </select>
          </div>

          <div class="form-group text-left">
            <input type="text" class="form-control" name="name" placeholder="{!! __('common.name') !!}*" required>
          </div>
          <div class="form-group text-left">
            <input type="email" class="form-control" name="email" placeholder="{!! __('common.email') !!}*" required>
          </div>
          <div class="form-group text-left">
            <input type="tel" class="form-control" name="tel" placeholder="{!! __('common.phone_number') !!}*" required>
          </div>
          <div class="form-group text-left">
            <textarea class="form-control" name="detail" rows="4" placeholder="{!! __('common.message') !!}" required></textarea>
          </div>

        </div>

        <div class="modal-footer">
          <button class="btn btn-primary w-100" type="submit">SEND</button>
        </div>

      </form>
    </div>
  </div>
</div>
