<div class="modal fade" id="modalFW" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog tac-vh-100" role="document">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span>Close</span>{!! file_get_contents(asset('/assets/images/icon-x.svg')) !!}
      </button>
    </div>
    <div class="modal-content">
      <div id="modalFWContent">
      </div>
    </div>
  </div>
</div>
