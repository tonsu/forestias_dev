@extends('layouts.master', [
  'body_class' => 'page-contact',
  'nav_title' => 'CONTACT US',
])

@section('title', 'Contact Us')

@section('content')

  <div class="nav-padding"></div>

  <div class="contact-bbg with-notice">
  </div>

  <div class="container">
    <div class="container-contact">
      <div class="contact-form-wrap">

        <div class="row">
          <div class="col col-12 col-lg-5 col-xl-4">

            <form id="formContact">
              <h2 class="color-primary">CONTACT US</h2>

              <div class="form-group text-left">
                <select class="form-control mdb-select md-form">
                  <option disabled selected hidden>Your Interest*</option>
                  <option value="whizdom_condominiums">Whizdom Condominiums</option>
                  <option value="mulberry_grove">Mulberry Grove</option>
                  <option value="aspen_tree_residences">The Aspen Tree Residences</option>
                  <option value="six_senses_residences">Six Senses Residences</option>
                </select>
              </div>

              <div class="form-group text-left">
                <input type="text" class="form-control" name="name" placeholder="Name*" required>
              </div>
              <div class="form-group text-left">
                <input type="email" class="form-control" name="email" placeholder="Email*" required>
              </div>
              <div class="form-group text-left">
                <input type="tel" class="form-control" name="tel" placeholder="Phone Number*" required>
              </div>
              <div class="form-group text-left">
                <textarea class="form-control" name="detail" rows="4" placeholder="Message" required></textarea>
              </div>

              <button class="btn btn-primary w-100" type="submit">SEND</button>

            </form>
          </div>

          <div class="col col-lg-7 col-xl-8 d-none d-lg-block con-form-text">
            <h3 class="color-primary">COMMUNITY<br/>& INTERGENERATIONAL</h3>
            <h4 class="color-3">Where Lasting Bonds are Created</h4>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section id="contact-info" class="last-section">
    <div class="container">
      <div class="container-contact">
        <h2 class="text-center">CONTACT <br class="d-md-none"/>INFORMATION</h2>
        <h4 class="text-center color-3">The Land of Everlasting Happiness</h4>
        <div class="tac-accordion acc-contact">
          <div class="acc-item">
            <div class="acc-head">
              <div class="marker-wrap">
                <img class="marker-active" src="{{asset('/assets/images/icon-marker-active.svg')}}"/>
                <img class="marker" src="{{asset('/assets/images/icon-marker.svg')}}"/>
              </div>
              <h3 class="color-primary">
                HEAD OFFICE OF MQDC
              </h3>
            </div>
            <div class="acc-content">
              <div class="acc-side">
                <div class="map-wrap">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.2696272993226!2d100.59120631511179!3d13.702112990379646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f939659e817%3A0xc3f44de43bf0cdad!2sMagnolia%20Quality%20Development%20Corporation%20Limited!5e0!3m2!1sen!2sth!4v1583913403468!5m2!1sen!2sth"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
                </div>
              </div>
              <p>
                Magnolia Quality Development Corporation Limited
                695 Soi Sukhumvit 50, Sukhumvit Rd. Phrakanong
                Klong Toey, Bangkok 10260
              </p>
              <p>Email: <strong class="color-3">theforestias@mqdc.com</strong></p>
              <p>Call Center:<a class="nav-call" href="tel:1265">1265</a></p>
            </div>
          </div>
          <div class="acc-item">
            <div class="acc-head">
              <div class="marker-wrap">
                <img class="marker-active" src="{{asset('/assets/images/icon-marker-active.svg')}}"/>
                <img class="marker" src="{{asset('/assets/images/icon-marker.svg')}}"/>
              </div>
              <h3 class="color-primary">
                THE FORESTIAS
              </h3>
            </div>
            <div class="acc-content">
              <div class="acc-side">
                <div class="map-wrap">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4778.167740665943!2d100.66745281473564!3d13.655684497623705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d5e4be711259d%3A0x4c51c714caeac071!2sTHE%20FORESTIAS!5e0!3m2!1sen!2sth!4v1583913620911!5m2!1sen!2sth"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
                </div>
              </div>
              <p>
                Magnolia Quality Development Corporation Limited
                695 Soi Sukhumvit 50, Sukhumvit Rd. Phrakanong
                Klong Toey, Bangkok 10260
              </p>
              <p>Email: <strong class="color-3">theforestias@mqdc.com</strong></p>
              <p>Call Center:<a class="nav-call" href="tel:1265">1265</a></p>
            </div>
          </div>
          <div class="acc-item">
            <div class="acc-head">
              <div class="marker-wrap">
                <img class="marker-active" src="{{asset('/assets/images/icon-marker-active.svg')}}"/>
                <img class="marker" src="{{asset('/assets/images/icon-marker.svg')}}"/>
              </div>
              <h3 class="color-primary">
                MQDC 上海品牌中心
              </h3>
            </div>
            <div class="acc-content">
              <div class="acc-side">
                <div class="map-wrap">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13647.782184744055!2d121.530439!3d31.222237999999997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa4ba2cd28e5bf91b!2sLujiazui%20Investment%20Tower%20Parking%20Lot!5e0!3m2!1sen!2sth!4v1583948341744!5m2!1sen!2sth"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
                </div>
              </div>
              <p>
                Magnolia Quality Development Corporation Limited
                695 Soi Sukhumvit 50, Sukhumvit Rd. Phrakanong
                Klong Toey, Bangkok 10260
              </p>
              <p>Email: <strong class="color-3">theforestias@mqdc.com</strong></p>
              <p>Call Center:<a class="nav-call" href="tel:1265">1265</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection
