@extends('layouts.master', [
  'body_class' => 'page-about',
  'nav_title' => __('about.nav_title'),
])

@section('title', __('about.title'))

@section('content')

  <div id="about-hero" class="hero-container tac-vh-100 nav-padding">
    <div class="bg-center-cover w-100 h-100 with-notice"
         style="background-image: url({{asset('/assets/images/about/banner-bg-about.jpg')}}"></div>
  </div>

  <section id="about-ceo" class="theme-light section-effect">
    <div class="container">
      <div class="ceo-content-wrap">
        <div class="ceo-content">
          {!! __('about.ceo_title') !!}
          <div class="ceo-img d-lg-none"
               style="background-image: url({{asset('/assets/images/about/ceo.jpg')}}"></div>
          <div class="ceo-quote">
            {!! __('about.ceo_content') !!}
          </div>
          <img class="ceo-signature" src="{{asset('/assets/images/about/ceo-signature.png')}}"/>
        </div>
      </div>
    </div>
  </section>

  <section id="about-mission" class="theme-light">
    <div class="container">
      <div class="am-content-wrap">
        <div class="am-content">
          <img class="am-logo" src="{{asset('assets/images/logo.svg')}}"/>
          {!! __('about.mission_title') !!}
          <div class="am-desc">
            {!! __('about.mission_content') !!}
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="about-concepts" class="section-effect">
    <div class="container">
      <div class="ac-content-wrap">
        <div class="ac-content">
          <h2 class="color-primary">
            {!! __('about.concept_title') !!}
          </h2>
          <h4 class="color-3">
            {!! __('about.concept_subtitle') !!}
          </h4>
          <div class="ac-desc">
            <div class="row">
              <div class="ac-item col col-6 col-lg-3">
                <img class="ac-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
                {!! __('about.concept_1') !!}
              </div>
              <div class="ac-item col col-6 col-lg-3">
                <img class="ac-logo" src="{{asset('assets/images/ficon-community.svg')}}"/>
                {!! __('about.concept_2') !!}
              </div>
              <div class="ac-item col col-6 col-lg-3">
                <img class="ac-logo" src="{{asset('assets/images/ficon-generations.svg')}}"/>
                {!! __('about.concept_3') !!}
              </div>
              <div class="ac-item col col-6 col-lg-3">
                <img class="ac-logo" src="{{asset('assets/images/ficon-sustainnovation.svg')}}"/>
                {!! __('about.concept_4') !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div id="about-content" class="last-section section-effect">
    <div class="container">

        <div class="about-section">
          <div class="as-img with-notice">
            <img class="w-100" src="{{asset('assets/images/about/about-nature.jpg')}}"/>
          </div>
          <div class="as-content">
            <img class="as-logo" src="{{asset('assets/images/ficon-nature.svg')}}"/>
            <h2 class="color-primary">
              {!! __('about.section_1_title') !!}
            </h2>
            {!! __('about.section_1_content') !!}
          </div>
        </div>
        <div class="about-section">
          <div class="as-img with-notice">
            <img class="w-100" src="{{asset('assets/images/about/about-community.jpg')}}"/>
          </div>
          <div class="as-content">
            <img class="as-logo" src="{{asset('assets/images/ficon-community.svg')}}"/>
            <h2 class="color-primary">
              {!! __('about.section_2_title') !!}
            </h2>
            {!! __('about.section_2_content') !!}
          </div>
        </div>
        <div class="about-section">
          <div class="as-img">
            <img class="w-100" src="{{asset('assets/images/about/about-generations.jpg')}}"/>
          </div>
          <div class="as-content">
            <img class="as-logo" src="{{asset('assets/images/ficon-generations.svg')}}"/>
            <h2 class="color-primary">
              {!! __('about.section_3_title') !!}
            </h2>
            {!! __('about.section_3_content') !!}
          </div>
        </div>
        <div class="about-section">
          <div class="as-img">
            <img class="w-100" src="{{asset('assets/images/about/about-sustainnovation.jpg')}}"/>
          </div>
          <div class="as-content">
            <img class="as-logo" src="{{asset('assets/images/ficon-sustainnovation.svg')}}"/>
            <h2 class="color-primary">
              {!! __('about.section_4_title') !!}
            </h2>
            {!! __('about.section_4_content') !!}
          </div>
        </div>
    </div>
  </div>
@endsection
