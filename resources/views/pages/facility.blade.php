@extends('layouts.master', [
  'body_class' => 'page-facility',
  'nav_title' => __('facilities.nav_title'),
])

@section('title', __('facilities.title'))

@section('content')

  @include('partials.facilities.hero')

  <section id="fac-medical" class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.medical_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.medical_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.medical')
      </div>
    </div>
  </section>

  <section class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.community_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.community_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.community')
      </div>
    </div>
  </section>

  <section class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.sixsenses_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.sixsenses_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.sixsenses')
      </div>
    </div>
  </section>

  <section class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.neighborhood_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.neighborhood_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.neighborhood')
      </div>
    </div>
  </section>

  <section class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.night_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.night_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.night')
      </div>
    </div>
  </section>

  <section id="fac-transport" class="fac-container theme-facility section-effect">
    <div class="container">
      <div class="fc-top row">
        <div class="fc-head col col-12 col-lg-7">
          {!! __('facilities.transport_title') !!}
        </div>
        <div class="fc-desc col col-12 col-lg-5">
          {!! __('facilities.transport_content') !!}
        </div>
      </div>
      <div class="fc-slide">
        @include('galleries.facilities.transport')
      </div>
      <div class="row ft-features">
        <div class="col col-12 col-lg-8 col-feature">
          <div class="icon-wrap bg-center-contain"
               style="background-image: url({{asset('/assets/images/ficon-bus.svg')}})"></div>
          {!! __('facilities.transport_desc_1') !!}
        </div>
        <div class="col col-12 col-lg-4 col-feature">
          <div class="icon-wrap bg-center-contain"
               style="background-image: url({{asset('/assets/images/ficon-eco.svg')}})"></div>
          {!! __('facilities.transport_desc_2') !!}
        </div>
      </div>
    </div>
  </section>

  <section id="fac-shows" class="theme-light">
    <div class="container section-effect">
      <div class="row">
        <div class="col col-12 col-lg-6">
          {!! __('facilities.shows_title') !!}
        </div>
        <div class="col col-12 col-lg-4 ml-lg-auto">
          {!! __('facilities.shows_content') !!}
        </div>
      </div>
    </div>

    @include('galleries.facilities.shows')
  </section>

  @include('partials.facilities.health')


@endsection
