@extends('layouts.master', [
  'body_class' => 'page-blog',
  'nav_title' => 'BLOG & NEWS',
])

@section('title', 'BLOG & NEWS')

@section('content')

  <div id="blog-hero" class="hero-container tac-vh-100 nav-padding">
    <div class="bg-center-cover w-100 h-100 with-notice" style="background-image: url({{asset
    ('/assets/images/blog/banner-bg-blog.jpg')}}"></div>
    <div class="hero-logo">
      <img src="{{asset('/assets/images/forestias-text.svg')}}" alt="logo"/>
      <p>The Enchanted Community District in the Forest</p>
    </div>
  </div>

  <div id="blog-story">
    <h2 class="color-primary text-center">OUR STORY</h2>
    <h4 class="color-3 text-center">The Land of Everlasting Happiness</h4>
    <div class="slide slide-center">
      <a href="{{route('single')}}" class="slide-item">
        <div class="slide-cover-image" style="background-image: url({{asset('/assets/images/home/banner-1.jpg')}}"></div>
        <div class="slide-content">
          <h3 class="text-uppercase color-primary">Community of Dream?</h3>
          <p>Imagine yourself got more siblings, relatives and living together as a warn and loving family. How happy will you be ?</p>
        </div>
      </a>
      <div class="slide-item" data-video="https://www.youtube.com/embed/K7pg6uJnbn4">
        <div class="slide-cover-image" style="background-image: url({{asset('/assets/images/home/banner-2.jpg')}}"></div>
        <div class="slide-content">
          <h3 class="text-uppercase color-primary">Community of Dream?</h3>
          <p>Imagine yourself got more siblings, relatives and living together as a warn and loving family. How happy will you be ?</p>
        </div>
      </div>
      <div class="slide-item" data-video="https://www.youtube.com/embed/JWt_73thoJ0">
        <div class="slide-cover-image" style="background-image: url({{asset('/assets/images/home/banner-3.jpg')}}"></div>
        <div class="slide-content">
          <h3 class="text-uppercase color-primary">Community of Dream?</h3>
          <p>Imagine yourself got more siblings. Imagine yourself got more siblings, relatives and living together as a warn and loving family. How happy will you be ?</p>
        </div>
      </div>
      <a href="{{route('single')}}" class="slide-item">
        <div class="slide-cover-image" style="background-image: url({{asset('/assets/images/home/banner-1.jpg')}}"></div>
        <div class="slide-content">
          <h3 class="text-uppercase color-primary">Community of Dream?</h3>
          <p>Imagine yourself got more siblings, relatives and living together as a warn and loving family. How happy will you be ?</p>
        </div>
      </a>
    </div>
  </div>

  <div id="blog-news" class="last-section">
    <div class="container">
      <h2 class="color-primary">BLOG & NEWS</h2>
      <div class="tac-tab">
        <div class="tt-tabs">
          <a href="#" class="tt-tab-item active">ALL</a>
          <a href="#" class="tt-tab-item">BLOG</a>
          <a href="#" class="tt-tab-item">NEWS</a>
        </div>
        <div class="tt-content">
          <div class="post-container" id="postContainer">
            @for ($i = 0; $i < 9; $i++)
              @component('post.item', [
              'date' => $randomDate = date("d M Y", mt_rand(1, time())),
              'type' => 'NEWS',
              'link' => route('single')
              ])
                @slot('thumbnail')
                  {{asset('/assets/images/home/banner-1.jpg')}}
                @endslot
                @slot('title')
                  <h3>COMMUNITY OF DREAM?</h3>
                @endslot
                @slot('excerpt')
                  <p>Whizdom The Forestias is the new definition of vertical living, with innovative design to meet your every daily need. Each detail forms the basis of perfect living..</p>
                @endslot
              @endcomponent
            @endfor
          </div>
          <div class="tac-pagination">
            <a class="tp-icon" href="#"><svg><use xlink:href="#lnr-first"></use></svg></a>
            <a class="tp-icon" href="#"><svg><use xlink:href="#lnr-chevron-left"></use></svg></a>
            <div class="tp-dots">...</div>
            <a class="tp-link active" href="#">2</a>
            <a class="tp-link" href="#">3</a>
            <a class="tp-link" href="#">4</a>
            <div class="tp-dots">...</div>
            <a class="tp-icon" href="#"><svg><use xlink:href="#lnr-chevron-right"></use></svg></a>
            <a class="tp-icon" href="#"><svg><use xlink:href="#lnr-last"></use></svg></a>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
