@extends('layouts.master', [
  'body_class' => 'page-404',
  'nav_title' => __('common.404_nav_title'),
])

@section('title', __('common.404_page_title'))

@section('content')
<style>
  .section-404 {
    width: 100%;
    height: 100%;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    font-family: "Sukhumvit Set", sans-serif;
    padding-left: 1.375em;
    padding-right: 1.375em;
  }
  .section-404 .notfound-content {
    text-align: center;
  }
  .section-404 h1 {
    font-size: 3.125em;
    color: #2A4634;
    margin: 0 0 1.2rem 0;
    line-height: 1;
    font-weight: 600;
  }
  .section-404 h3 {
    font-size: 1.125em;
    margin: 0 0 2.5rem 0;
    font-weight: normal;
    line-height: 1.4;
  }
  .section-404 .btn {
    height: 2.75rem;
    letter-spacing: 0.1em;
    padding: 0.7rem 1.2rem;
    min-width: 8em;
    z-index: 3;
    outline: 0;
    transition: all 0.2s;
    font-size: 1em;
  }
  .section-404 .btn:hover {
    opacity: .5;
  }
  .section-404 .btn-wrap {
    align-self: center;
  }
  .section-404 .btn-secondary {
    background-color: #7f7f75;
    border-color: #79796e;
    color: #ffffff !important;
    transition: opacity .2s;
  }
  .section-404 .btn-secondary:hover {
    opacity: .5;
  }
  .section-404 img {
    width: 8.875em;
    height: auto;
    margin-bottom: 3em;
  }

  .page-404 #sideButtons {
    display: none;
  }

  @media only screen and (max-width: 767px) {
    .section-404 h1 {
      font-size: 1.75em;
      margin: 0 0 .8rem 0;
    }
    .section-404 h3 {
      font-size: 1.125em;
      margin: 0 0 3rem 0;
    }
    .section-404 img {
      width: 5.875em;
      margin: 0 0 3rem 0;
    }
  }



</style>
<section class="section-404 tac-vh-100 nav-padding">
  <div class="notfound-content">
    <div class="404-logo">
      <img src="{{asset('/assets/images/logo.svg')}}" />
    </div>
    <h1>{!! __('common.404_title') !!}</h1>
    <h3>{!! __('common.404_subtitle') !!}</h3>
    <div class="btn-wrap">
      <a href="{{ route('index') }}" class="btn btn-secondary">
        {!! __('common.404_back_home_caption') !!}
      </a>
    </div>
  </div>
</section>
@endsection
