@extends('layouts.master', [
  'body_class' => 'page-family',
  'nav_title' => __('family.nav_title'),
])

@section('title', __('family.title'))

@section('content')

  @include('partials.family.hero')

  <div class="last-section">
  @include('projects.whizdom')
  @include('projects.mulberry')
  @include('projects.aspen')
  @include('projects.sixsenses')
  </div>

@endsection
