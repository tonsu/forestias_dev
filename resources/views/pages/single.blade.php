@extends('layouts.master', [
  'body_class' => 'page-single',
  'nav_title' => 'BLOG & NEWS',
])

@section('title', 'BLOG & NEWS')

@section('content')

  <div class="nav-padding"></div>

  <div id="single-banner"></div>

  <div id="single-content">
    <div class="container">
      <div class="single-container">
        <h2 class="color-primary text-uppercase">COMMUNITY OF DREAM?</h2>
        <h5 class="single-info color-secondary">
          Publish : 10 Jul 2018 | <span class="text-uppercase">NEWS</span>
        </h5>
        <div class="single-socials">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank"
             class="social-item">
            <svg>
              <use xlink:href="#tic-facebook"></use>
            </svg>
          </a>
          <a href="http://twitter.com/share?url={{url()->current()}}" target="_blank" class="social-item">
            <svg>
              <use xlink:href="#tic-twitter"></use>
            </svg>
          </a>
          <a href="#" class="social-item" target="_blank">
            <svg>
              <use xlink:href="#tic-link"></use>
            </svg>
          </a>
        </div>
        <div class="entry-content">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
            Aldus
            PageMaker including versions of Lorem Ipsum.</p>
          <figure>
            <img src="{{asset('/assets/images/home/banner-1.jpg')}}" alt="alt text">
            <figcaption>This is image caption.</figcaption>
          </figure>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
          <ul>
            <li>List one</li>
            <li>List two</li>
            <li>List three</li>
          </ul>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
          <h1>Heading 1</h1>
          <h2>Heading 2</h2>
          <h3>Heading 3</h3>
          <h4>Heading 4</h4>
          <h5>Heading 5</h5>
        </div>
        <div class="single-tags">
          <strong>TAGS:</strong>
          <div class="tag-content">
            <span>#Glow</span><span>#Glow</span><span>#Glow</span>
            <span>#Glow</span><span>#Glow</span><span>#Glow</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="single-back" class="d-sm-none">
    <div class="container">
      <div class="btn-back">
        <svg>
          <use xlink:href="#tic-back"></use>
        </svg>
        <span>BACK</span>
      </div>
    </div>
  </div>

  <div id="single-related" class="last-section">
    <div class="container">
      <hr class="sr-hr"/>
      <h2 class="color-primary text-uppercase">Related News</h2>
      <div class="slide slide-related post-container">
        @for ($i = 0; $i < 3; $i++)
          @component('post.item', [
          'date' => $randomDate = date("d M Y", mt_rand(1, time())),
          'type' => 'NEWS',
          'link' => route('single')
          ])
            @slot('thumbnail')
              {{asset('/assets/images/home/banner-1.jpg')}}
            @endslot
            @slot('title')
              <h3>COMMUNITY OF DREAM?</h3>
            @endslot
            @slot('excerpt')
              <p>Whizdom The Forestias is the new definition of vertical living, with innovative design to meet your
                every daily need. Each detail forms the basis of perfect living..</p>
            @endslot
          @endcomponent
        @endfor
      </div>
    </div>
  </div>

@endsection
