@extends('layouts.master', ['body_class' => 'page-home'])

@section('title', __('home.title'))

@section('content')

  @include('partials.home.hero')

  <!-- Section: Community -->
  <section id="home-community" class="theme-light section-effect">
    <div class="container">
      <div class="row">
        <div class="col col-12 col-lg-8">
          {!! __('home.community_title') !!}
        </div>
        <div class="col col-12 col-lg-4">
          {!! __('home.community_content') !!}
        </div>
      </div>
      @include('galleries.home.community')
    </div>
  </section>

  <!-- Section: Nature -->
  <section id="home-nature" class="theme-light section-effect">
    <div class="container">
      <div class="text-center">
        {!! __('home.nature_title') !!}
        <div class="section-content">
          {!! __('home.nature_content') !!}
        </div>
      </div>
      @include('galleries.home.nature')
    </div>
  </section>

  @include('partials.home.project')

  <div id="home-projects">
  @include('projects.whizdom')
  @include('projects.mulberry')
  @include('projects.aspen')
  @include('projects.sixsenses')
  </div>

  @include('partials.home.forest')

  @include('partials.home.walkingtrails')

@endsection
