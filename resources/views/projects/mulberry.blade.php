<section id="project-mulberry" class="project-item section-effect">
  <div class="container">
    <div class="project-top">
      {{-- Description --}}
      <div class="project-content">
        <h2 class="color-primary">{!! __('family.mulberry_title') !!}</h2>
        <h3 class="color-secondary">{!! __('family.mulberry_subtitle') !!}</h3>
        {!! __('family.mulberry_content') !!}
        <a href="{!! __('family.mulberry_link') !!}" class="btn btn-secondary">
          {!! __('common.more_detail') !!}
        </a>
      </div>
      {{-- Banner --}}
      <div class="project-banner">
        <img class="w-100" src="{{asset('/assets/images/project/project-mulberry.jpg')}}"/>
      </div>
    </div>
    {{-- Carousel --}}
    @include('galleries/family/mulberry')
  </div>
</section>
