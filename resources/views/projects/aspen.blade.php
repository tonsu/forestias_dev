<section id="project-aspen" class="project-item section-effect">
  <div class="container">
    <div class="project-top">
      {{-- Banner --}}
      <div class="project-banner">
        <img class="w-100" src="{{asset('/assets/images/project/project-aspen.jpg')}}"/>
      </div>
      {{-- Description --}}
      <div class="project-content">
        <h2 class="color-primary">{!! __('family.aspen_title') !!}</h2>
        <h3 class="color-secondary">{!! __('family.aspen_subtitle') !!}</h3>
        {!! __('family.aspen_content') !!}
        <a href="{!! __('family.aspen_link') !!}" class="btn btn-secondary">
          {!! __('common.more_detail') !!}
        </a>
      </div>
    </div>
    {{-- Carousel --}}
    @include('galleries/family/aspen')
  </div>
</section>
