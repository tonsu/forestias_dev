<section id="project-sixsenses" class="project-item section-effect">
  <div class="container">
    <div class="project-top">
      {{-- Description --}}
      <div class="project-content">
        <h2 class="color-primary">{!! __('family.sixsenses_title') !!}</h2>
        <h3 class="color-secondary">{!! __('family.sixsenses_subtitle') !!}</h3>
        {!! __('family.sixsenses_content') !!}
        <a href="{!! __('family.sixsenses_link') !!}" class="btn btn-secondary">
          {!! __('common.more_detail') !!}
        </a>
      </div>
      {{-- Banner --}}
      <div class="project-banner">
        <img class="w-100" src="{{asset('/assets/images/project/project-sixsenses.jpg')}}"/>
      </div>
    </div>
    {{-- Carousel --}}
    @include('galleries/family/sixsenses')
  </div>
</section>
