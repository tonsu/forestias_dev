<section id="project-whizdom" class="project-item section-effect">
  <div class="container">
    <div class="project-top">
      {{-- Banner --}}
      <div class="project-banner">
        <img class="w-100" src="{{asset('/assets/images/project/project-whizdom.jpg')}}"/>
      </div>
      {{-- Description --}}
      <div class="project-content">
        <h2 class="color-primary">{!! __('family.whizdom_title') !!}</h2>
        <h3 class="color-secondary">{!! __('family.whizdom_subtitle') !!}</h3>
        {!! __('family.whizdom_content') !!}
        <a href="{!! __('family.whizdom_link') !!}" class="btn btn-secondary">
          {!! __('common.more_detail') !!}
        </a>
      </div>
    </div>
    {{-- Carousel --}}
    @include('galleries/family/whizdom')
  </div>
</section>
