<a href="{{ $link }}" class="post-item col col-12 col-md-6 col-hd-4">
  <div class="pi-cover bg-center-cover" style="background-image: url({{ $thumbnail }}"></div>
  <div class="pi-content">
    {{ $title }}
    {{ $excerpt }}
  </div>
  <div class="pi-info">
    Publish
    @unless(empty($date))
      {{ $date }}
    @endunless
    @unless(empty($type))
      | <span class="text-uppercase">{{ $type }}</span>
    @endunless
  </div>
</a>

