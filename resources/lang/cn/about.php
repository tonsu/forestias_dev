<?php
return [
  'title' => 'THE FORESTIAS - ABOUT',
  'nav_title' => 'ABOUT US',

  'ceo_title' =>
    '<h2>CEO愿景</h2>
     <h4>总裁对未来社区的愿景</h4>',
  'ceo_content' =>
    '<h5>
      我们的愿景是The Forestias成为快乐人居环境的首个范本，
    </h5>
    <p>
       树立代际共居社区的优秀模范。 The Forestias致力于成为引领居民与自然和谐共处，将自然生态系统融入人类社区的最佳实践。
    </p>',

  'mission_title' => '<h2>我们的使命</h2>',
  'mission_content' =>
    '<p>在森林绿洲内打造一个新的代际家庭社区，为社区内的居民和游客服务，帮助每一代人重建与世间万物的联系。 
    </p>
    <p>这个社区将通过模范住宅区、邻里社区、以及居住环境的设计来建立联系，让居民幸福一生。
    </p>
    <p>The Forestias将通过自然、学习、服务、便利设施、健康和休闲等多方精心配套设施，培养一个由居民和游客组成的多元化社区。
    </p>',

  'concept_title' =>
    '可持续幸福的四大理念',
  'concept_subtitle' => 'the foundation for<br class="d-md-none" /> building your future happiness',
  'concept_1' => '<p>自然的500种色彩</p>',
  'concept_2' => '<p>梦想中的社区</p>',
  'concept_3' => '<p>四代同堂</p>',
  'concept_4' => '<p>SUSTAINNOVATION<br/>FOR WELL-BEING</p>',

  'section_1_title' => '自然的500种色彩',
  'section_1_content' =>
    '<p>
      大自然丰富多彩，生机勃勃，曾未与世人如此亲近过。这座“现代丛林”拥有500多种动植物，整个项目因其而多姿多彩。精心设计4个栖息地，居民重归大自然。从宁静的花园，到密林边缘，在这片生态系统中，你会重获生活与自然步调一致的喜悦。
    </p>',
  'section_2_title' => '梦想中的社区',
  'section_2_content' =>
    '<p>
      当家庭中各代齐聚时，幸福随之而来。社区内的活动令人欢呼踊跃，人们谈笑风生，相安相受，彼此分享交流，代沟消弭于无形。
    </p>',
  'section_3_title' => '四代同堂',
  'section_3_content' =>
    '<p>
      我们乐于打造可供四代人分享彼此真挚情感的住宅空间。住宅内精心设计，适宜每一代人的生活方式。居家环境舒适宜人，活动种类丰富，家人其乐融融，欢聚一堂。
    </p>',
  'section_4_title' => '可持续创新的幸福',
  'section_4_content' =>
    '<p>
      采用新颖的想法和全新的技术，将自然融入到日常生活中，提升居民、居住环境，甚至是整个地球的福祉。我们将此领域定义为“可持续创新的幸福”。MQDC研究人员热情工作，以使人类在这块福地上重归自然，让 一切皆欣欣向荣。
    </p>',
];
