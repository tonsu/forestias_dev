<?php
return [
  'title' => 'THE FORESTIAS - FACILITIES',
  'nav_title' => 'FACILITIES',

  'hero_image' => 'bg-banner-fac-full.jpg',

  // Medical

  'medical_title' =>
    '<h2>国际一流医院</h2>',
  'medical_content' =>
    '<p>
      在这里，规划的世界级水平的医院，各种便利设施完善为居民提供服务：350张床位，配备国家级医护人员，提供国际医疗服务水平，这些都将为居民的健康提供坚实保障，让生活无后顾之忧。
    </p>',
  'medical_caption' => 'MEDICAL COMPLEX',

  // Community

  'community_title' =>
    '<h2>社区中心&家庭中心</h2>',
  'community_content' =>
    '<p>
      社区中心，是居民互动的社交空间。各类与The Forestias理念一致的社区商店，都将以现代多元化的生活要素，为居民和游客提供前沿生活方式。
    </p>',
  'community_caption' => '社区中心&家庭中心',

  // Six Senses

  'sixsenses_title' =>
    '<h2>六感酒店、度假村和水疗中心，推崇健康生活理念</h2>',
  'sixsenses_content' =>
    '<p>
      生活在这里的人，都可以享受到六感酒店、度假村和水疗中心的健康设施和服务等特别优待。从20世纪90年代初，六感酒店因确立了酒店行业的早期服务基准得到广泛认可。六感酒店和The Forestias都将致力于推崇健康的生活、宜居的社区及可持续设计等理念。
    </p>',
  'sixsenses_caption' => '六感酒店、度假村和水疗中心，推崇健康生活理念',

  // Neighborhood

  'neighborhood_title' =>
    '<h2>社区宜居更宜人</h2>',
  'neighborhood_content' =>
    '<p>
        我们旨在创造一个适合所有年龄段生活的宜居社区，通过提供现代化的便利设施，营造自然的社交环境，使家人的关系能够更为融洽且亲密。所有的公共和社区空间都完全相连，可通过空中栈道轻松到达，这意味着路上花费的时间更短，让您和家人朋友永远不会因为距离而疏远。
      </p>',
  'neighborhood_caption' => '社区宜居更宜人',

  // Magical Night

  'night_title' =>
    '<h2>The Forestias奇妙夜</h2>',
  'night_content' =>
    '<p>
      The Forestias将定期举办壮观迷人的夜间表演。与亲朋好友、左邻右舍一起观赏和探索黑夜中的奇妙光芒，让其点亮您的想象之灯。
    </p>',
  'night_caption' => 'The Forestias奇妙夜',

  // Transport

  'transport_title' =>
    '<h2>交通&行动便利</h2>',

  'transport_content' =>
    '<p>
      可持续交通是指安全的、可靠的多式交通，既可以增强社区间的连动性，又可以很好的连接The Forestias社区内的所有居民。
     </p>',
  'transport_desc_1' =>
    '<p>
      The Forestias社区的内部道路，道路宽敞完善，旨在为居民、工作人员和公众提供巴士接驳服务，以缓解拥堵问题。为了减少私家车的使用及空气污染，接驳巴士将设有许多接送站点，高效地连接住宅零售区、医疗中心和其他公共区域。公交路线和站点时间可通过应用程序访问，以便居民轻松计划日常通勤。
    </p>',
  'transport_desc_2' =>
    '<p>
      无车区设有电动汽车充电站，该设计鼓励居民使用其他出行方式，例如步行、慢跑、骑行或无污染电动汽车等替代交通工具出行。
    </p>',
  'transport_caption' => '行动便利',

  // Shows & Performances

  'shows_title' =>
    '<h2>季节性游行娱乐表演</h2>',
  'shows_content' =>
    '<p>
      每家每户都可观赏季节性娱乐表演，包括缤纷嘉年华、万圣节集会、圣诞派对和不尽其数的宴会。一年中的欢乐时刻，尽在The Forestias。
    </p>',
  'shows_caption' =>'季节性游行娱乐表演',

  // Health

  'health_title' =>
    '<h2>HEALTH, WELL-BEING & HAPPINESS</h2>
     <h4>Designed for Health and Happiness</h4>',
  'health_1_title' => 'COMMUNITY OF DREAMS',
  'health_1_subtitle' => 'EVERYDAY LIFE AT<br class="d-none d-lg-inline"/> THE FORESTIAS',
  'health_1_content' => '一年365天， The Forestias让您每天都能发现生活的特别之处，带来非同一般的新奇体验。',

  'health_2_title' => 'Connecting',
  'health_2_subtitle' => 'INTERGENERATIONAL<br/> COMMUNITY<br/> THROUGH NATURE',
  'health_2_content' => '自然的魅力无人能抵。结合自然景观进行设计，将森林环境与代际社区紧密连接，万物和谐共生。The Forestias将为家庭、自然及野生动植物提供一个幸福的安心之所。 ',

  'health_3_title' => 'EDUCATION & DISCOVERY',
  'health_3_subtitle' => 'OBSERVING VARIOUS<br/> SPECIES OF BIRDS<br/> AND THEIR BEHAVIORS',
  'health_3_content' => '在The Forestias的森林，还将打造一个户外教室。孩子们将在自然环境中学习传统技能。建造户外教室的优点在于，让孩子们在开阔的环境中自由活动，既可以高效地获取知识，又可以锻炼身体，增强身体抵抗力。专家们还将鼓励孩子们走近野生森林，激发他们探索大自然的兴趣，共同分享生物学，植物学，昆虫学，鸟类学，艺术与设计以及社会责任等方面的宝贵见解。',

  'health_4_title' => 'LOVE TOGETHER. HAPPY TOGETHER.',
  'health_4_subtitle' => 'THE FORESTIAS<br/> LEARNING CENTER',
  'health_4_content' => '在The Forestias，可持续学习是重中之重。学习中心会聘请曼谷最好学校和机构的老师给3至18岁的学生授课，学生们根据自己的特长设计课程表。同时社区中心也会开办针对成年人的讲习班和课程，让居民发掘自己的才能，获取更多新的技能。',

];
