<?php
return [
  'title' => 'THE FORESTIAS',
  'nav_title' => '',

  'hero_subtitle' => '令人沉醉的森林世界',

  'community_title' =>
    '<h2>THE FORESTIAS COMMUNITY</h2>
     <h4>令人沉醉的森林世界</h4>',
  'community_content' =>
    '<p>
      欢迎来到The Forestias，一个与自然深度交融，人、动物、植物和谐共生的社区。
    </p>',

  'nature_title' =>
    '<h2>回归自然 </h2>
     <h4>Living in Harmony with Nature</h4>',
  'nature_content' =>
    '<p>
      大自然对人类既有养育作用，又有治愈的能力。所以亲近自然，体验大自然给予的无限奇妙与欢乐，<br class="d-none d-lg-inline"/>
      感受万物一体，和谐共生，也是人类保持身心健康的重要途径之一。
    </p><p>
      我们创新方法思路，运用可持续设计和建造，让自然直接呈现于眼前，让人类深拥自然，让自然滋养人类。<br class="d-none d-lg-inline"/>
      欢迎各位与我们一起，踏上一段激动人心的旅程，探索一种全新的生活方式，并以平和的心态怀抱自然与现代化，<br class="d-none d-lg-inline"/>
      将平衡注入生活之中，让生活的每一刻都收获相应的满足感。
    </p>',

  'portfolio_title' => '多品系住宅综合体社区让各种生活方式得以完满实现。',

  'forest_title' =>
    '<h2>FOREST<br class="d-sm-none"> AT THE FORESTIAS</h2>',
  'forest_content' =>
    '<p>
      在社区的中心位置，规划的森林面积将超过30莱，即48,000平方米。该区域植被丰富，其中森林和绿地覆盖面积将占项目景观比例约45%。
      森林将具体分为以下几个区域：
    </p>',


  'forest_1_title' => '密林区',
  'forest_1_content' =>
    '<p>
      密林区覆盖面积为3.75莱（6,000平方米），植被成熟，高大茂密，属于森林的自然保护区。为保护自然的原始性，该区域禁止游客进入，只允许野生动物栖息和护林员踏足。
    </p>',
  'forest_2_title' => '森林馆',
  'forest_2_content' =>
    '<p>
      这是The Forestias的生态系统学习中心，未来将向学生和公众开放，以帮助人们进行研究和教育活动。
    </p>',
  'forest_3_title' => '住宅区',
  'forest_3_content' =>
    '<p>
      住宅楼周围的森林和绿地，可以用来供居民进行社区活动及休闲娱乐。空中栈道，将连接所有住宅区和公园，漫步其中，可以感受到阳光穿过树叶，光影斑驳的视觉美感。
    </p>',
  'forest_4_title' => '活动草坪',
  'forest_4_content' =>
    '<p>
      这片开放区域将举办生态教育活动，如通过露营自然，来观察和研究树木、花卉、野生动物和生态系统等。
    </p>',

  'view_walking_trails' => '步道',

  'walking_trail_image' => 'map-walking-trails-mb.jpg',
  'walking_trail_image_mb' => 'map-walking-trails-mb.jpg',
  'walking_trail_title' => '<h3>WALKING TRAILS</h3>',
  'walking_trail_content' =>
    '<p>
      The Forestias拥有丰富多样的生态景点，便利设施一应俱全。居民和访客，不仅可以享受社区提供的丰富户外活动，还可以安全愉悦地探索森林，与动植物和谐相处。
    </p>
    <p>
      The Forestias拥有丰富多样的生态景点，便利设施一应俱全。居民和访客，不仅可以享受社区提供的丰富户外活动，还可以安全愉悦地探索森林，与动植物和谐相处。
    </p>
    <p>
      社区内修建了空中栈道，高度为8—12米，对公众开放。这条栈道悬建在树干之间，居民和访客可以在此位置观赏森林的茂密丛林，发现许多有趣的树木、花朵和地面上看不到的动物。
    </p>',

  'walking_trail_legend_red' => '空中栈道（公共开放）',
  'walking_trail_legend_orange' => '空中栈道（只限居民）',
  'walking_trail_legend_gray' => '主步道',

];
