<?php
return [
  'title' => 'THE FORESTIAS - FAMILY',
  'nav_title' => 'FAMILY',

  'hero_title' => 'A portfolio of projects<br/>to complement every lifestyle.',

  'whizdom_title' => 'WHIZDOM<br/>THE FORESTIAS',
  'whizdom_subtitle' => 'THE NEW DEFINITION OF VERTICAL LIVING',
  'whizdom_link' => '#',
  'whizdom_content' =>
    '<p>
      Whizdom The Forestias重新诠释垂直人居，利用设计创新完善生活。郁郁森林，绿意盎然，下一代与自然连结更为紧密。臻于细节，精于品质，高质生活带来可持续幸福。与此同时，城市中心近在咫尺，设施配置适于每种生活方式。
    </p>',

  'mulberry_title' => 'MULBERRY GROVE<br/>THE FORESTIAS',
  'mulberry_subtitle' => 'SUPER-LUXURY RESIDENCES',
  'mulberry_link' => '#',
  'mulberry_content' =>
    '<p>
      MULBERRY GROVE超豪华住宅秉承“培育代际幸福生活”理念，满足不同生活方式，实现代际共居。住宅设施齐全，精心设计，可与家人和乐融融，齐聚一堂，也可行至一隅，享受独处时光。我们认为，唯有家庭稳固和谐，才能感受真正的幸福。
    </p>',

  'aspen_title' => 'THE ASPEN TREE<br/>THE FORESTIAS',
  'aspen_subtitle' => 'SUPER-LUXURY RESIDENCES',
  'aspen_link' => '#',
  'aspen_content' =>
    '<p>
      The Aspen Tree 专为高龄人士打造，全方位贴心照顾。这是泰国首家提出“居家养老”概念的项目。通过“通用设计”和“智能家居”技术，家居环境舒适宜人，居民身心健康，幸福长寿。
    </p>',

  'sixsenses_title' => 'SIX SENSES RESICENCES<br/>THE FORESTIAS',
  'sixsenses_subtitle' => 'SUPER EXCLUSIVE RESIDENTIAL VILLAS',
  'sixsenses_link' => '#',
  'sixsenses_content' =>
    '<p>
      The Forestias六感住宅区，包括27个别墅，由世界著名的酒店品牌六感酒店进行管理。这些豪宅采用泰国热带建筑风格设计，室内与室外景致紧密相连；打造自然通风系统，营造轻松的居住氛围。生活在这里的居民，可以从房间的任何一个角度欣赏到室外花草树木、湖泊、绿茵草地等风光景致，还可以在附近的六感酒店度假区和水疗中心，享受特别优惠及服务。
    </p>',
];
