<?php
return [
  'image_disclaimer' => 'Computer generated imagery for advertising purposes only',
  'image_disclaimer_2' => '内容和图片仅限推广使用。在法律范围内, 公司有权在不事先通知的情况下更改项目细节。',
  'image_disclaimer_footer' => '内容和图片仅限推广使用。在法律范围内, 公司有权在不事先通知的情况下更改项目细节。',
  'more_detail' => 'MORE DETAIL',
  'register' => 'REGISTER',
  'send' => 'SEND',
  'close' => 'Close',
  'download_brochure' => 'DOWNLOAD BROCHURE',

  'your_interest' => 'Your Interest',
  'name' => 'Name',
  'email' => 'Email',
  'phone_number' => 'Phone Number',
  'message' => 'Message',

  // Toggle Register Modal On Load
  'register_modal_show_default' => 'mobile,desktop', // possible value: { 'mobile', 'desktop', 'mobile,desktop', '' }
];
