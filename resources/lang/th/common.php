<?php
return [
  'image_disclaimer' => 'Computer generated imagery for advertising purposes only',
  'image_disclaimer_2' => 'ข้อความและภาพจำลองเพื่อการใช้โฆษณาเท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงรายละเอียด โดยไม่ต้องแจ้งให้ทราบล่วงหน้า',
  'image_disclaimer_footer' => 'ข้อความและภาพจำลองเพื่อการใช้โฆษณาเท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงรายละเอียด โดยไม่ต้องแจ้งให้ทราบล่วงหน้า',
  'more_detail' => 'MORE DETAIL',
  'register' => 'REGISTER',
  'send' => 'SEND',
  'close' => 'Close',
  'download_brochure' => 'DOWNLOAD BROCHURE',

  'your_interest' => 'Your Interest',
  'name' => 'Name',
  'email' => 'Email',
  'phone_number' => 'Phone Number',
  'message' => 'Message',

  // Toggle Register Modal On Load
  'register_modal_show_default' => 'mobile,desktop', // possible value: { 'mobile', 'desktop', 'mobile,desktop', '' }
];
