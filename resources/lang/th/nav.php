<?php
return [
  'home' => 'HOME',
  'about' => 'ABOUT US',
  'facilities' => 'FACILITIES',
  'family' => 'FAMILY',
  'contact' => 'CONTACT US',
  'blog' => 'BLOG & NEWS',
  'subscribe' => 'SUBSCRIBE',
  'subscribe_title' => 'BE THE FIRST TO HEAR ABOUT THE FORESTIAS',
  'subscribe_placeholder' => 'Enter your email address',
  'call_center' => 'CALL CENTER',
  'copyright' => 'Copyright ©' . date("Y") .' THE FORESTIAS. All rights reserved.'
];
