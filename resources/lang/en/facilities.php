<?php
return [
  'title' => 'THE FORESTIAS - FACILITIES',
  'nav_title' => 'FACILITIES',

  'hero_image' => 'bg-banner-fac-full.jpg',

  // Medical

  'medical_title' =>
    '<h2>MEDICAL COMPLEX</h2>',
  'medical_content' =>
    '<p>
      A world-class hospital with about 350 beds staffed by physicians with national reputations and providing
      medical services to an international standard.
    </p>',
  'medical_caption' => 'MEDICAL COMPLEX',

  // Community

  'community_title' =>
    '<h2>COMMUNITY CENTER<br/>& FAMILY CENTER</h2>',
  'community_content' =>
    '<p>
      The Town Center is a social space for residents to interact. Shops in tune with the concept of The Forestias
      will provide a cutting-edge lifestyle for residents and visitors with elements like a vintage cinema and
      leading tutorial schools. The Experience Zone will offer organic restaurants providing tasty food, healthy
      drinks, homemade baked goods, and vegan options. Plant and flower shops, holistic clinics, fitness centers,
      and various kinds of market are also in our plan.
    </p>',
  'community_caption' => 'COMMUNITY CENTER & FAMILY CENTER',

  // Six Senses

  'sixsenses_title' =>
    '<h2>SIX SENSES<br/>THE FORESTIAS</h2>',
  'sixsenses_content' =>
    '<p>
      Reconnect and reinvigorate your senses in places of incredible natural beauty, with meaningful experiences, empathetic hospitality, and pioneering wellness woven into the fabric of Six Senses The Forestias. At Six Senses The Forestias, guests can enjoy a tranquil haven in easy reach from Bangkok\'s multitude of cultural and leisure attractions.
    </p>',
  'sixsenses_caption' => 'SIX SENSES THE FORESTIAS',

  // Neighborhood

  'neighborhood_title' =>
    '<h2>LIVABLE<br/>NEIGHBORHOOD</h2>',
  'neighborhood_content' =>
    '<p>
        Our aim is to create a livable neighborhood for all ages, where modern amenities bring families closer in a
        natural social setting. All public and community spaces are fully connected by a Residence Sky Walk, so you
        spend less time traveling and are never far from family and friends.
      </p>',
  'neighborhood_caption' => 'LIVABLE NEIGHBORHOOD',

  // Magical Night

  'night_title' =>
    '<h2>MAGICAL NIGHT<br/>AT THE FORESTIAS</h2>',
  'night_content' =>
    '<p>
      The Forestias will regularly hold enchanting nighttime displays.
      Let the magical lighting spark your imagination as you explore the wonderful illuminations with family,
      friends, and neighbors.
    </p>',
  'night_caption' => 'MAGICAL NIGHT AT THE FORESTIAS',

  // Transport

  'transport_title' =>
    '<h2>TRANSPORT & ACTIVE MOBILITY</h2>',
  'transport_content' =>
    '<p>
      Sustainable transportation means safe, reliable, multi-modal transportation that enhances the localcommunity and
      provides connectivity for all residents within The Forestias.
     </p>',
  'transport_desc_1' =>
    '<p>
      Internal roads within The Forestias are spacious and designed to ease congestion with shuttle bus services
      available for residents,
      staff, and the public. Aiming to reduce the use of private cars and lessen airborne pollution, shuttle buses
      will connect residential,
      retail, and medical complexes to other public areas efficiently with numerous pick-up and drop-off points.
      Bus routes and timing are
      available through an application so you can easily plan your daily commute.
    </p>',
  'transport_desc_2' =>
    '<p>
      A car free zone with EV charging stations has also
      been designed to encourage residents to use
      alternative transportation such as walking, jogging,
      biking, or nonpolluting electric vehicles.
    </p>',
  'transport_caption' => '',

  // Shows & Performances

  'shows_title' =>
    '<h2>SEASONAL PARADES OF SHOWS<br class="d-none d-fhd-inline"/> AND PERFORMANCES</h2>',
  'shows_content' =>
    '<p>
      Everyone in the family can enjoy the fun of seasonal shows and celebrations. Funfairs and parties will
      bring joy at Halloween, Christmas, and other festivals. Get set for year-round entertainment
      at The Forestias.
    </p>',
  'shows_caption' => 'SEASONAL SHOWS AND FESTIVALS',
  'shows_img_content' => '<p>SEASONAL SHOWS AND FESTIVALS</p>',

  // Health

  'health_title' =>
    '<h2>HEALTH, WELL-BEING<br class="d-lg-none"/> & HAPPINESS</h2>',
  'health_1_subtitle' => 'COMMUNITY OF DREAMS',
  'health_1_title' => 'EVERYDAY LIFE AT<br class="d-none d-lg-inline"/> THE FORESTIAS',
  'health_1_content' => 'For all the 365 days of the year, there are
endless experiences to discover at
The Forestias, where each day is special.',
  'health_1_caption' => 'EVERYDAY LIFE AT THE FORESTIAS',

  'health_2_subtitle' => 'CONNECTING',
  'health_2_title' => 'INTERGENERATIONAL<br class="d-none d-lg-inline"/> COMMUNITY<br class="d-none d-lg-inline"/> THROUGH NATURE',
  'health_2_content' => 'Because happiness and nature are at the heart of our development, we understand the importance of providing an environment that enriches family and social bonds across different generations. With many intergenerational spaces for activity and leisure, The Forestias hopes to build a community where lifelong memories and bonds of love are created daily as you grow closer to each other and to nature.
',
  'health_2_caption' => 'CONNECTING INTERGENERATIONAL COMMUNITY THROUGH NATURE',

  'health_3_subtitle' => 'EDUCATION & DISCOVERY',
  'health_3_title' => 'OBSERVING VARIOUS<br class="d-none d-lg-inline"/> SPECIES OF BIRDS<br class="d-none d-lg-block"/> AND THEIR BEHAVIORS',
  'health_3_content' => 'We believe that discovering and experiencing the wonders
of nature first hand will instill a lifelong appreciation for the
ecosystem and love for conservation.',
  'health_3_caption' => 'EDUCATION & DISCOVERY',

  'health_4_subtitle' => 'LOVE TOGETHER. HAPPY TOGETHER.',
  'health_4_title' => 'THE FORESTIAS<br class="d-none d-lg-inline"/> LEARNING CENTER',
  'health_4_content' => 'Sustainable learning is always a priority at The Forestias. The Learning Center will offer individual and
group tuition from teachers at Bangkok’s finest schools and institutes for students aged 3–18. Pupils
will get to design their own schedules and excel in their chosen subjects. Workshops and classes for
adults will also let residents explore their talents and discover new skills.',
  'health_4_caption' => 'THE FORESTIAS LEARNING CENTER',

];
