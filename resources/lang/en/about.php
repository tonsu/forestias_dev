<?php
return [
  'title' => 'THE FORESTIAS - ABOUT',
  'nav_title' => 'ABOUT US',

  'ceo_title' =>
    '<h2>OUR CEO’S VISION</h2>
     <h4>FOR THE COMMUNITY OF TOMORROW</h4>',
  'ceo_content' =>
    '<h5>
      Our vision for The Forestias is the premiere<br class="d-none d-xl-inline"/>
      model of a happy living environment,
    </h5>
    <p>
      providing the finest intergenerational family community. The Forestias aims to lead the world in best 
      practice in living with nature, reintegrating natural ecosystems within the human community.
    </p>',

  'mission_title' => '<h2>OUR MISSION</h2>',
  'mission_content' =>
    '<p>To create a new intergenerational family community within a forested oasis that will serve its residents
      and
      visitors, helping each generation and all living beings to reconnect.
    </p>
    <p>This community will foster lifelong happiness, building connections through exemplary residential,
      neighborhood,
      and environmental design.
    </p>
    <p>The Forestias will nurture a diverse community of residents and visitors through nature, learning,
      services,
      amenities, wellness, and leisure.
    </p>',

  'concept_title' =>
    '4 CONCEPTS<br class="d-md-none" />
    OF SUSTAINABLE<br class="d-md-none" />
    HAPPINESS',
  'concept_subtitle' => 'for<br class="d-md-none" /> building your future',
  'concept_1' => '<p>500 SHADES<br/>OF NATURE</p>',
  'concept_2' => '<p>COMMUNITY<br/>OF DREAMS</p>',
  'concept_3' => '<p>CONNECTING<br/>4 GENERATIONS</p>',
  'concept_4' => '<p>SUSTAINNOVATION<br/>FOR WELL-BEING</p>',

  'section_1_title' => '500 SHADES<br class="d-lg-none"/> OF NATURE',
  'section_1_content' =>
    '<p>
      Nature in all its richness and color lives here alongside humans as never before. The entire project is
      enriched with the many hues of a ‘modern jungle’, with over 500 species of plant and animal. We are
      bringing humankind back to the natural world in 4 meticulously conceived ‘habitats. Ranging from a
      tranquil garden, to the edge of a dense forest, these ecosystems will let you rediscover the joy of living
      in tune with nature.
    </p>',
  'section_2_title' => 'COMMUNITY<br class="d-lg-none"/> OF DREAMS',
  'section_2_content' =>
    '<p>
      Happiness ensues when all generations, of a family come together. Activities within the community engage everyone, swapping ideas and helping one another, building connections and bridging the gap between generations.
    </p>',
  'section_3_title' => 'CONNECTING<br/> 4 GENERATIONS',
  'section_3_content' =>
    '<p>
      We enjoy designing places where 4, generations can share ever-deepening love and understanding. We have crafted amenities to suit the lifestyles of each generation. And we have designed a myriad activities to build fond memories and foster warm relationships in happy, sustainable homes.
    </p>',
  'section_4_title' => 'SUSTAINNOVATION<br/> FOR WELL-BEING',
  'section_4_content' =>
    '<p>
      Bringing nature back into our lives takes fresh ideas and new technologies to enhance the well-being of
      people, place, and planet. This is a field we term "sustainnovation\'. MQDC\'s researchers have worked passionately to bring humankind back into nature at a place where all living beings can thrive.
    </p>',
];
