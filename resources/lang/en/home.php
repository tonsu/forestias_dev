<?php
return [
  'title' => 'THE FORESTIAS',
  'nav_title' => '',

  'hero_subtitle' => 'The Land of Everlasting Happiness',

  'community_title' =>
    '<h2>THE FORESTIAS COMMUNITY</h2>
     <h4>The Enchanted Community District in the Forest</h4>',
  'community_content' =>
    '<p>
      Welcome to The Forestias. A community in profound harmony with the natural world. A place where human, animals and nature live harmoniously together.
    </p>',
  'community_caption' => 'RESIDENT FOREST',

  'nature_title' =>
    '<h2>COME HOME TO NATURE</h2>
     <h4>Living in Harmony with Nature</h4>',
  'nature_content' =>
    '<p>
      We know that nature has a therapeutic and nurturing influence on people of all ages mentally and physically.<br class="d-none d-lg-inline"/>
      Being close to nature reminds us of the wonders and joys of the natural world and how<br class="d-none d-lg-inline"/>
      the balance of nature creates harmony for all living beings.
    </p><p>
      We will nurture this connection by bringing nature right to your home through our innovative use of<br class="d-none d-lg-inline"/>
      sustainable design and construction. Join us on an exciting journey to discover a new way of living, one<br class="d-none d-lg-inline"/>
      that equally embraces nature and modernity to bring a sense of balance and fulfillment to every moment of
      your life.
    </p>',
  'nature_caption' => 'RESIDENT FOREST',

  'portfolio_title' => 'A portfolio of projects<br/>to complement every lifestyle.',

  'forest_title' =>
    '<h2>FOREST<br class="d-sm-none"> AT THE FORESTIAS</h2>',
  'forest_content' =>
    '<p>
      Forest will cover more than 30 rai, or 48,000 sqm, at the heart of the project. Developed through intensive
      planting, forest and green space will make up 45% of the project landscape. These areas will be divided into the
      following zones.
    </p>',

  'forest_map' => 'map-forest.png',
  'forest_map_mb' => 'map-forest-mb.jpg',

  'forest_1_title' => 'DEEP FOREST',
  'forest_1_caption' => 'DEEP FOREST',
  'forest_1_content' =>
    '<p>
      Deep forest will cover 3.75 rai with dense mature trees and vegetation. Only wildlife and forest rangers will step foot in this reserve for nature.
    </p>',
  'forest_2_title' => 'FOREST PAVILION',
  'forest_2_caption' => 'FOREST PAVILION',
  'forest_2_content' =>
    '<p>
      The Forestias ecosystem learning center will be open to students and members of the public for research and education.
    </p>',
  'forest_3_title' => 'RESIDENT FOREST',
  'forest_3_caption' => 'RESIDENT FOREST',
  'forest_3_content' =>
    '<p>
      Forest and green areas around residential buildings will have community and leisure uses. The Residence
      Sky Walk will connect all the residential zones and parks with a walkway through the forest canopy.
    </p>',
  'forest_4_title' => 'EVENT LAWN',
  'forest_4_caption' => 'EVENT LAWN',
  'forest_4_content' =>
    '<p>
      This open area will host eco-educational activities such as nature camps or observing and studying trees and flowers, wildlife, and ecosystems.
    </p>',

  'view_walking_trails' => 'VIEWS WALKING TRAILS',

  'walking_trail_image' => 'map-walking-trails.jpg',
  'walking_trail_image_mb' => 'map-walking-trails-mb.jpg',
  'walking_trail_title' => '<h3>WALKING TRAILS</h3>',
  'walking_trail_content' =>
    '<p>
      The Forestias is a complete theme destination with many eco attractions and all the conveniences of the city
      within your reach in a secure, natural environment. There are many outdoor activities available for residents and
      visitors to enjoy as they explore the forest and its animals safely and happily.
    </p>
    <p>
      The Resident Forest consists of exclusive forest and green zones for residents within the development to be
      used for family, social, community or leisure activities. All zones are interconnected via a Residence Sky Walk, making
      travel between the different sites easy and effortless.
    </p>
    <p>
      A Canopy Walk 8-12 meters above ground is open to the public. This walkway among the treetops allows visitors
      and residents to admire the canopy level of the forest where many interesting trees, flowers, and animals not seen
      on ground level can be found.
    </p>',

  'walking_trail_legend_red' => 'Canopy walk (public)',
  'walking_trail_legend_orange' => 'Canopy walk (residents)',
  'walking_trail_legend_gray' => 'Main walkway',

];
