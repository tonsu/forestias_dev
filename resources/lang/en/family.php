<?php
return [
  'title' => 'THE FORESTIAS - FAMILY',
  'nav_title' => 'FAMILY',

  'hero_title' => 'A portfolio of projects<br/>to complement every lifestyle.',

  'whizdom_title' => 'WHIZDOM<br/>THE FORESTIAS',
  'whizdom_subtitle' => 'THE NEW DEFINITION OF VERTICAL LIVING',
  'whizdom_link' => '#',
  'whizdom_content' =>
    '<p>
      Consisting of 3 high-rise apartment buildings, Destinia, Mytopia, and Petopia, Whizdom is about a holistic well-being design approach to vertica living. Seamless integrational of a modern vertical living lifestyle within a natural environment for optimum well-being encourages resdents to “Feel the Wild”. All buildings have large outdoor parks and community spaces for family and pets to play in and enjoy the fresh air.
    </p>',
  'whizdom_caption' => 'WHIZDOM THE FORESTIAS',

  'mulberry_title' => 'MULBERRY GROVE<br/>THE FORESTIAS',
  'mulberry_subtitle' => 'SUPER-LUXURY RESIDENCES',
  'mulberry_link' => '#',
  'mulberry_content' =>
    '<p>
      “MULBERRY GROVE” Super-luxury residences under the concept of
      “Nurturing Intergenerational Happiness” will bring together every generation in the family to spend time
      together, while ensuring the privacy that every age requires. Because we believe that strong families are
      the foundation of true happiness.
    </p>',
  'mulberry_caption' => 'MULBERRY GROVE VILLAS THE FORESTIAS',

  'aspen_title' => 'THE ASPEN TREE<br/>THE FORESTIAS',
  'aspen_subtitle' => 'THE AGING-IN-PLACE COMMITMENT',
  'aspen_link' => '#',
  'aspen_content' =>
    '<p>
      The Aspen Tree’s residences are not just places to live. They are
homes for a lifetime. The aging-in-place concept lets you live in
your own home and community with the greatest independence
and comfort, whatever your age or your health needs. Your
current and future needs inform every aspect of design, from
residence layouts to the holistic lifetime care that underwrites
your lifestyle, ensuring you always receive the support you need to
stay healthy, happy, and independent.
    </p>',
  'aspen_caption' => 'THE ASPEN TREE THE FORESTIAS',

  'sixsenses_title' => 'SIX SENSES RESICENCES<br/>THE FORESTIAS',
  'sixsenses_subtitle' => 'SUPER EXCLUSIVE RESIDENTIAL VILLAS',
  'sixsenses_link' => '#',
  'sixsenses_content' =>
    '<p>
      Six Senses Residences are 27 super-exclusive villas managed by the world-renowned Six Senses hospitality brand. These grand residences are designed in a Tropical Thai architectural style, with a focus on seamlessly blending indoor and outdoor living spaces to create a naturally airy and relaxing atmosphere. The residences come with gorgeous views of the forest, lake, and surrounding gardens. Residents also enjoy special privileges at the nearby Six Senses hotel resorts and spa.
    </p>',
  'sixsenses_caption' => 'SIX SENSES RESIDENCES THE FORESTIAS',
];
