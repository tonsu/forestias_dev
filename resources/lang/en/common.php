<?php
return [
  'image_disclaimer' => 'Computer generated imagery for advertising purposes only',
  'image_disclaimer_2' => 'Computer-generated imagery for advertising purposes only.<br class="d-md-none"/> Serviced Apartments is a future projects (details are subject to change).',
  'image_disclaimer_footer' => 'All text and images are for advertising purposes only. The company reserves the right to amend the details specified herein without prior notice.',
  'more_detail' => 'MORE DETAIL',
  'register' => 'REGISTER',
  'send' => 'SEND',
  'close' => 'Close',
  'download_brochure' => 'DOWNLOAD BROCHURE',
  'download_brochure_link' => '/assets/downloads/THE FORESTIAS SALEBOOK.pdf',

  'your_interest' => 'Your Interest',
  'name' => 'Name',
  'email' => 'Email',
  'phone_number' => 'Phone Number',
  'message' => 'Message',

  // 404 page
  '404_title' => 'PAGE NOT FOUND',
  '404_subtitle' => 'Try refreshing your search or use the navigation below to return to the main page.',
  '404_back_home_caption' => 'GO BACK TO HOME',
  '404_nav_title' => '',
  '404_page_title' => 'THE FORESTIAS',


  // Toggle Register Modal On Load
  'register_modal_show_default' => 'mobile,desktop', // possible value: { 'mobile', 'desktop', 'mobile,desktop', '' }
];
