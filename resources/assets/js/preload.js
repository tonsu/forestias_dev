export default function (callback) {

  let waitInterval = setInterval(waitLoading, 1000)
  let waitCount = 0

  function waitLoading () {
    let $body = $('body')
    let $preloader = $('#preloader')
    let $scrollContainer = $('#scrollContainer')
    waitCount++
    if (waitCount > 60 || $body.hasClass('loaded')) { // force finish (second)
      $body.addClass('loaded')
      clearInterval(waitInterval)
      $scrollContainer.css({
        visibility: 'visible',
        opacity: 0,
      })
      callback()
      setTimeout(function () {
        $preloader.css({
          opacity: 0
        })
      }, 500)
      setTimeout(function () {
        $scrollContainer.css({
          transition: 'opacity .5s',
          opacity: 1,
        })
      }, 1000)
      setTimeout(function () {
        $preloader.css({
          display: 'none',
        })
      }, 1500)
    }
  }
}
