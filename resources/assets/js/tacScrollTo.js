import easing from './easing'

export default function () {
  let $scrollContainer = $('#scrollContainer')
  let $scrollElem = $scrollContainer.length ? $scrollContainer : $('html,body')
  let minScrollDuration = 1000
  let maxScrollDuration = 3000
  let $body = $('body')
  let $stickyNav = $('#stickyNav')
  let navStickyStart = $('#home-about').length && $('#home-about').offset().top || 0

  window.tacScrollTo = function (selector, animate = true) {
    // Debugging
    // console.log('[tac-scroll]', selector);
    // console.log('$scrollElem', $scrollElem);
    let top = -1
    if (typeof selector === 'number') {
      top = selector
    } else {
      if (selector !== '#' && $(selector).offset()) {
        top = $(selector).offset().top + 1 // +1 to prevent unexpected gap
        if(window.scrollBar) {
          top += window.scrollBar.scrollTop
        }
      }
    }
    if (!animate) {
      if (window.scrollBar) {
        scrollBar.scrollTo(0, top)
      } else {
        $scrollElem.scrollTop(top)
      }
    } else {
      let duration = minScrollDuration
      // sticky nav
      if ($stickyNav.height() && (top - 1) > navStickyStart) {
        top -= $stickyNav.height()
      }
      if (top > -1) {
        // Note: can add the curve here
        duration = Math.abs($scrollElem.scrollTop() - top) / 6
      }
      // Limiting duration
      if (!duration || duration < minScrollDuration) {
        duration = minScrollDuration
      } else if (duration > maxScrollDuration) {
        duration = maxScrollDuration
      }
      if (window.scrollBar) {
        scrollBar.scrollTo(0, top, duration, { easing: easing.easeOutQuart })
      } else {
        $scrollElem.animate({ scrollTop: top }, duration, 'easeOutQuart')
      }
    }
  }

  // Custom Scroll Menu
  // ----------------------------------------
  function initScrollMenu () {
    let $links = $('a[href^="#"]')
    $links.click(function (e) {
      e.preventDefault()
      if ($body.hasClass('home') && !$body.hasClass('hero-scroll-finish')) {
        doHeroScroll($(this).attr('href'))
      } else {
        tacScrollTo($(this).attr('href'))
      }
    });
    let menuSecPos = []
    $('.nav-container').first().find('a').each(function () {
      var id = $(this).attr('href')
      if (id.length > 1 && id.indexOf('#') >= 0) {
        if ($(id).offset()) {
          menuSecPos.push({
            id: id,
            offset: $(id).offset().top,
          })
        }
      }
    })
  }

  initScrollMenu()
}

