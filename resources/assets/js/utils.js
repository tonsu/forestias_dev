/**
 * Get offset relative to the document.
 * @returns {{top: number, left: number}}
 */
// export function offset (el) {
export function offsetWithoutSmoothScrollbar (el) {
  var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return {
    top: rect.top + scrollTop,
    left: rect.left + scrollLeft,
    right: document.body.clientWidth - (rect.left + rect.width)
  }
}

// Use this if using it with SmoothScrollbar
export function offset (el) {
  var rect = el.getBoundingClientRect(),
    scrollTop = window.scrollBar && window.scrollBar.scrollTop || 0
  return {
    top: rect.top + scrollTop,
  }
}

let ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

export function getUID (length) {
  var rtn = '';
  for (var i = 0; i < length; i++) {
    rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
  }
  return rtn;
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export function debounce (func, wait, immediate) {
  var timeout;
  return function () {
    var context = this, args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

export function hasClass (el, className) {
  if (el.classList)
    return el.classList.contains(className);
  return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
}

export function addClass (el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className))
    el.className += " " + className;
}

export function removeClass (el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
    el.className = el.className.replace(reg, ' ');
  }
}

export function removeStyles(el) {
  el.removeAttribute('style');

  if(el.childNodes.length > 0) {
    for(var child in el.childNodes) {
      /* filter element nodes only */
      if(el.childNodes[child].nodeType == 1)
        removeStyles(el.childNodes[child]);
    }
  }
}

/**
 * time: millisecond
 */
export function setCookie(cname, cvalue, extime) {
  var d = new Date();
  // d.setTime(d.getTime() + (exdays*24*60*60*1000));
  d.setTime(d.getTime() + extime);
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export const breakpoints = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  hd: 1600,
  fhd: 1900,
  qhd: 2500,
}
