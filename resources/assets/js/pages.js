import * as u from "./utils"

export default function () {

  // Facility : Hero
  // ----------------------------------------
  let $body = $('body')
  let $facHeroBanner = $('#facHeroBanner')
  let $heroSwipe = $('<div id="heroSwipe"></div>')
  if ($facHeroBanner.length) {
    // remember the original dimension
    let elImg = document.querySelector('#facHeroBanner img')
    let $heroSection = $('#fac-hero')
    $facHeroBanner.data('ratio', elImg.naturalWidth / elImg.naturalHeight)
    let $parent = $facHeroBanner.parent('#facHeroBannerCnt')
    let lastWidth = 0

    // onSwipe
    let onTouch = u.debounce(function () {
      $heroSection.removeClass('has-swipe')
    }, 250)
    $parent[0].addEventListener("touchmove", onTouch, false);

    let onWindowResize = u.debounce(function () {
      let ratio = $facHeroBanner.data('ratio')
      if (window.innerWidth / window.innerHeight < ratio) {
        let height = $parent.outerHeight()
        let width = height * ratio
        $facHeroBanner.css({
          height: height,
          width: width,
          margin: 0,
        })
        // Prevent centering when only height was changed on device
        if (lastWidth !== width) {
          $parent.css({
            overflow: 'scroll',
            overflowY: 'hidden',
          })
        }
        $parent.scrollLeft((width - window.innerWidth) / 2.5) // not actually centered
        lastWidth = width
        $heroSection.addClass('has-swipe')
      } else {
        $facHeroBanner.removeAttr('style')
        $parent.removeAttr('style')
        $heroSection.removeClass('has-swipe')
      }
    }, 250);
    // $facHeroBanner.append($heroSwipe.wrap('<div class="swipe-wrap"></div>'))
    $heroSection.append($heroSwipe)
    window.addEventListener("resize", onWindowResize)
  }

  // Eliminating the need to double add the description manually.
  // Facility : some sections
  let $facContainers = $('.fac-container')
  if ($facContainers.length) {
    $facContainers.each(function () {
      let me = $(this)
      let descCloned = me.find('.fc-desc').clone()
      descCloned.removeClass().addClass('fc-desc-cloned')
      me.find('.container').append(descCloned)
    })
  }

  // Facility : transport (further edited)
  let $facTransport = $('#fac-transport')
  if ($facTransport.length) {
    let descCloned = $facTransport.find('.fc-desc-cloned')
    $facTransport.find('.ft-features').before(descCloned)
  }

  // Facility : shows
  let $facShows = $('#fac-shows')
  if ($facShows.length) {
    $facShows.each(function () {
      let me = $(this)
      let descCloned = me.find('.col-lg-4').clone()
      descCloned.removeClass().addClass('fc-desc-cloned')
      me.append(descCloned)
    })
  }

  // About - Mobile Content Generation
  // ----------------------------------------
  let $aboutSections = $('.about-section')
  if ($aboutSections.length) {
    $aboutSections.each(function () {
      let me = $(this)
      let cloned = me.find('.as-img').clone()
      cloned.removeClass('as-img').addClass('as-img-cloned')
      me.find('h2').after(cloned)
    })
  }

  // Projects -  Mobile Content Generation
  // ----------------------------------------
  let $portItems = $('.project-item')
  if ($portItems.length) {
    $portItems.each(function () {
      let me = $(this)
      let cloned = me.find('.project-banner').clone()
      cloned.removeClass('.project-banner').addClass('project-banner-mb')
      me.find('h3').after(cloned)
    })
  }


  // Home : Forest - Mobile Content Generation
  // ----------------------------------------
  let $homeForest = $('#home-forest')
  if ($homeForest.length) {
    let $mbContainer = $('<div class="forest-content-mb d-lg-none"</div>')
    // let $slideContainer = $('<div class="slide d-lg-none" ' +
    //   'data-opt-dots="true" data-opt-nav="false"></div>')
    // $mbContainer.append($slideContainer)
    // $slideContainer.append($homeForest.find('.fc-col').clone())
    $homeForest.find('.forest-content').after($mbContainer)
    let $clonedViewBtn = $homeForest.find('.btn-view-trails').clone()
    $clonedViewBtn.removeClass('d-none d-lg-block').addClass('d-lg-none')
    $mbContainer.append($clonedViewBtn)

    // The map
    let $map = $('.forest-map')

  }

  // Home : Forest - Modal
  // ----------------------------------------
  let $walkingTrails = $('#walkingTrails')
  if ($walkingTrails.length) {
    $('.btn-view-trails').click(function (event) {
      $('#modalFW').modal('toggle');
      $('#modalFWContent').append($('#walkingTrails'))
      event.preventDefault();
    })
  }

  // Home / Family : Projects
  // ----------------------------------------
  let $projectSelect = $('.project-select')
  if ($projectSelect.length) {

    let $navDynamicContent = $('#navDynamicContent')
    $navDynamicContent.html($projectSelect.clone())

    if ($body.hasClass('page-home')) {
      let $homeProjectCnt = $('#home-projects')
      let $allProjectSelect
      $('.project-select-item').click(function () {
        let me = $(this)
        let index = me.index() + 1
        $('.project-select-item.active').removeClass('active')
        $('.project-select-item:nth-child(' + index + ')').addClass('active')
        tacScrollTo('#home-projects')

        $homeProjectCnt.find()
        $homeProjectCnt.css({
          opacity: 0,
          transition: 'opacity .2s',
        })
        setTimeout(function () {
          $homeProjectCnt.children('.project-item:nth-child(' + index + ')').addClass('active').siblings().removeClass('active')
          window.dispatchEvent(new Event('resize'))
        }, 300)
        setTimeout(function () {
          $homeProjectCnt.css({
            opacity: 1,
            transition: 'opacity .2s',
          })
        }, 700)

      })

      // set default
      let defaultIndex = 1
      $('.project-select-item:nth-child(' + defaultIndex + ')').addClass('active')
      $homeProjectCnt.children('.project-item:nth-child(' + defaultIndex + ')').addClass('active')
    }

    // Family : Project
    // ----------------------------------------
    else if ($body.hasClass('page-family')) {
      let $famProjects = $('.page-family .project-select-item')
      $famProjects.click(function () {
        tacScrollTo($(this).data('target'))
      })
    }
  }


  // Facilities : Health & Well being
  // ----------------------------------------
  let $galleryTrigger = $('.gallery-trigger')
  $galleryTrigger.click(function (e) {
    e.stopPropagation()
    let $gal = $(this).find('.gallery-container .gallery-thumb')
    if ($gal.length) {
      $gal[0].click()
    }
  })

  // Mobile
  let $healthContentMb = $('.health-content-mb')
  let $healthGalleryThumb = $('#fac-health .gallery-trigger')
  if ($healthContentMb.length) {
    let items = $healthContentMb.find('.slide-item')
    items.each(function (idx) {
      let me = $(this)
      me.data('index', idx)
      me.find('.forestias-img').click(function () {
        $healthGalleryThumb.eq(me.data('index')).click()
      })
    })
  }

}
