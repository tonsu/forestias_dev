import * as u from "./utils"

export default function () {

  // Setting active state
  // ----------------------------------------

  let nav = document.querySelector('.nav-container')
  if(nav) {
    let currentLanguageEl = nav.querySelector('.nav-langs a[data-locale="'+ php_vars.locale +'"]')
    u.addClass(currentLanguageEl, 'current')
    try {
      let lastUrlSegment = window.location.href.substring(window.location.href.lastIndexOf('/') + 1)
      lastUrlSegment = lastUrlSegment.split('?')[0]
      let links = nav.querySelectorAll('.nav-links a')
      for (let link of links) {
        let segments = link.href.split('/')
        if(segments[segments.length - 1] === lastUrlSegment) {
          u.addClass(link, 'current')
        }
      }
    } catch (e) {
      console.error('Cannot set current navigation.')
    }
  }
}
