window.Popper = require('popper.js').default
window.$ = window.jQuery = require('jquery')
import 'bootstrap'
import './bootstrap'
import './mobileTabletCheck'
import './jQuery.easing'
import './tacScrollTo'

import initPreload from './preload'
import initNav from './nav'
import initPages from './pages'
import initTacAccordion from './tacAccordion'
import initTacScroller from './tacScroller'
import initTacScrollTo from './tacScrollTo'
import initCarousel from './carousel'
import initPhotoViewer from './photoviewer'
import * as u from './utils'

// Effectively forget the chrome scroll position
// Required for production
// if ('scrollRestoration' in history) {
//   history.scrollRestoration = 'manual';
// }

jQuery(document).ready(function ($) {

  var $body = $('body')

  if (isMobile === true) {
    $body.addClass('is-mobile')
  }

  var locale = php_vars && php_vars.locale
  if (locale) {
    $body.addClass('lang-' + locale)
  }
  // Init pages first
  // Reason: must generates component's dom first.
  initPreload(initAll)

  function initAll () {
    initPages()
    // Then init components
    initTacAccordion()
    initPhotoViewer()
    initCarousel()
    initTacScrollTo()
    initTacScroller()
    initNav()

    // Modal
    // ----------------------------------------
    $("#formRegister").submit(function (event) {
      $('#modalRegister').modal('toggle');
      event.preventDefault();
    });

    // Nav
    // ----------------------------------------
    var $menuCheckbox = $('#menuCheckbox')
    $('#navBackdrop').click(function (e) {
      $menuCheckbox.trigger('click');
    });

    $('#backToTop').click(function () {
      tacScrollTo(0)
    })

    // Images
    // ----------------------------------------

    var imageDisclaimer = (php_vars && php_vars.text_image_disclaimer)
      || 'Computer generated imagery for advertising purposes only'

    var imageDisclaimer2 = (php_vars && php_vars.text_image_disclaimer_2)
      || 'Computer generated imagery for advertising purposes only'

    /**
     * Note: deprecated. Please use .forestias-img instead.
     */
    $('.img-caption').each(function () {
      var me = $(this)
      me.html('<div class="caption-text">' + me.html() + '</div>')
      me.append('<div class="caption-notice">' + imageDisclaimer + '</div>')
    })

    $('.with-notice').each(function () {
      $(this).append('<div class="img-caption">' +
        '<div class="caption-notice">' + imageDisclaimer + '</div>' +
        '</div>'
      )
    })

    $('.with-notice-2').each(function () {
      $(this).append('<div class="img-caption">' +
        '<div class="caption-notice">' + imageDisclaimer2 + '</div>' +
        '</div>'
      )
    })

    /**
     * Auto generate the image with disclaimer
     * ---
     * To get the benefit of the Forestias framework.
     * Used for single image and images inside the carousel.
     */
    $('.forestias-img').each(function () {
      var me = $(this)
      var $figCaption = me.children('figcaption')
      var withCaption = me.hasClass('with-disclaimer')


      if ($figCaption.length) {
        var html = $figCaption.html()
        $figCaption.html('<div class="caption-text">' + html + '</div> ' +
          (withCaption ? '<div class="caption-disclaimer">' + imageDisclaimer + '</div>' : ''))
      } else if (withCaption) {
        me.append('<figcaption>' +
          '<div class="caption-disclaimer">' + imageDisclaimer + '</div>' +
          '</figcaption>')
      } else {
        // remove the gradient
        me.addClass('no-caption')
      }
    })

    // Resize
    // ----------------------------------------
    var onWindowResize = u.debounce(function () {
      var i
      let syncRight = document.querySelectorAll('.sync-right')
      let menuToggle = document.getElementById('menuToggle')
      let menuToggleOffset = u.offset(menuToggle)
      for (i = 0; i < syncRight.length; ++i) {
        syncRight[i].style.right = menuToggleOffset.right;
      }

      $('.slide-center, .slide-centered').each(function () {
        try {
          let me = $(this)
          me.find('.slide-nav').height(
            me.find('.slide-cover-image, figure').first().outerHeight())
        } catch (e) {
          // ignore
        }
      })
    }, 250);
    window.addEventListener("resize", onWindowResize)

    /**
     * Trigger Windows Resize
     * This fix many things (e.g. carousel)
     */
    setTimeout(function () {
      window.dispatchEvent(new Event('resize'))
    }, 500)

    // Modal
    // ----------------------------------------
    let $videoIframe = $('#videoIframe')
    $('#modalVideo').on('hidden.bs.modal', function () {
      $videoIframe.attr('src', '')
    })

    // Show The Resister Modal
    // ----------------------------------------
    if ($body.hasClass('page-home')) {
      let cookieShowStatus = u.getCookie('show_register_onload')
      if (cookieShowStatus !== 'hide') {
        let showRegisterModalConfig = (php_vars && php_vars.register_modal_show_default) || ''
        let showRegisterModal = false
        if (isMobile) {
          if (showRegisterModalConfig.indexOf('mobile') > -1) showRegisterModal = true
        } else {
          if (showRegisterModalConfig.indexOf('desktop') > -1) showRegisterModal = true
        }
        if (showRegisterModal) {
          if (!cookieShowStatus) {
            u.setCookie('show_register_onload', 'hide', 1 * 24 * 60 * 60 * 1000)
          }
          setTimeout(function () {
            // Front-end Dev Note: this one-liner is how to trigger register modal
            $('#modalRegister').modal('toggle');
          }, 1500)
        }
      }
    }

    // Adding Register to the side
    // ----------------------------------------
    let textRegister = (php_vars && php_vars.text_register) || 'REGISTER'
    let $sideRegisterBtn = $('<button id="sideRegisterBtn" class="btn btn-primary" ' +
      'data-toggle="modal" data-target="#modalRegister">' + textRegister + '</button>')
    $('#sideButtons').append($sideRegisterBtn)

    // Debugging
    // ----------------------------------------
    // $('#modalRegister').modal('toggle');
    // $('#menuCheckbox').attr('checked', 'checked')
    // setTimeout(function () {
    //   tacScrollTo(90000, false)
    // }, 1500)
  }


});
