import 'owl.carousel2';

export default function () {

  // Carousel
  // ----------------------------------------
  let $slide = $('.slide')
  let $videoIframe = $('#videoIframe')
  $slide.each(function () {
    let me = $(this)
    let isCentered = me.hasClass('slide-center') || me.hasClass('slide-centered')
    let startPosition = 0
    let isLoop = true // false (change to true as the client wanting the loop for all)
    let hasDot = me.data('opt-dots') === true || me.hasClass('has-dots')
    let itemCount = isCentered ? 2 : 1
    let hasNav = !me.hasClass('slide-related') && me.data('opt-nav') !== false && !me.hasClass('no-nav')
    let autoHeight = me.data('opt-auto-height') !== false
    let $counter = null
    let autoplay = true
    let autoplayTimeout = (php_vars && php_vars.autoplayTimeout) || 6000 // todo
    let autoplayHoverPause = true
    me.addClass('owl-carousel')

    // Before carousel
    me.children().each(function () {
      let videoUrl = $(this).data('video')
      let $coverImage = $(this).find('.slide-cover-image')
      if (videoUrl) {
        let $playBtn = $('<div class="video-play"></div>')
        $coverImage.addClass('has-video').append($playBtn)
        $playBtn.click(function () {
          $videoIframe.attr('src', videoUrl + '?autoplay=1')
          $('#modalVideo').modal('toggle');
        })
      }
    })

    if (isCentered) {
      isLoop = true
      hasDot = true
      startPosition = me.children().length > 2 ? 1 : 0
    }

    let responsive = {}

    if (me.hasClass('slide-related')) {
      hasDot = true
      // 992, 768, 576
      responsive[768] = {
        dots: hasDot,
        startPosition: 0,
        items: 3
      }
      responsive[576] = {
        dots: hasDot,
        startPosition: 0,
        items: 2
      }
    } else {
      responsive[576] = {
        dots: hasDot,
        startPosition: startPosition,
        items: itemCount,
      }
    }

    // Make it carousel
    me.owlCarousel({
      nav: false,
      dots: hasDot,
      autoHeight: autoHeight,
      loop: isLoop,
      navText: ['', ''],
      center: isCentered,
      items: 1,
      responsive: responsive,
      autoplay: autoplay,
      autoplayTimeout: autoplayTimeout,
      autoplayHoverPause: autoplayHoverPause,
    })

    $counter = $('<div class="slide-counter"></div>')
    me.append($counter)

    // After carousel
    if (hasNav) {
      let $prev = $('<button class="slide-nav btn-prev"></button>')
      let $next = $('<button class="slide-nav btn-next"></button>')
      me.append($prev)
      me.append($next)

      $prev.click(function () {
        me.trigger('prev.owl.carousel')
      });
      $next.click(function () {
        me.trigger('next.owl.carousel')
      });

      function initOwlButtons (e) {
        if(e.item.count <= 1) {
          me.addClass('slide-one-item')
        } else if (!isLoop && e.item.count > 1) {
          me.removeClass('slide-one-item')
          if ((e.item.index + e.page.size) >= e.item.count) {
            $next.attr('disabled', 'disabled');
          } else {
            $next.removeAttr('disabled');
          }
          if (e.item.index === 0) {
            $prev.attr('disabled', 'disabled');
          } else {
            $prev.removeAttr('disabled');
          }
        }
        document.activeElement.blur() // prevent hover on dot (touch device)
        if($counter && e.item.count > 1) {
          var carousel = e.relatedTarget;
          $counter.text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length); // fix wrong index the looping
          // $counter.html(e.item.index + 1 + '/' + e.item.count); // before changed to looping
        }
      }

      me.on('changed.owl.carousel', initOwlButtons)
      me.on('refreshed.owl.carousel', initOwlButtons)
      me.trigger('refresh.owl.carousel')

      // Fix mouse leave not resuming autoplay
      // see: https://stackoverflow.com/questions/44614207/owl-carousel-doesnt-resume-on-mouse-hoverout
      me.on('mouseleave',function(){
        me.trigger('stop.owl.autoplay'); //this is main line to fix it
        me.trigger('play.owl.autoplay', [autoplayTimeout]);
      })

      // Pause until scrolled to
      me.trigger('stop.owl.autoplay');

      if (isCentered) {
        me.append('<div class="slide-center-bg"></div>')
      }
    }

  })
}
