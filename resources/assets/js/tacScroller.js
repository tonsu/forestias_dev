import Scrollbar from 'smooth-scrollbar'
import * as u from "./utils"

export default function () {
  let debugScrollbar = false

  let scrollContainer = document.getElementById('scrollContainer')
  let otherScrollContainers = document.querySelectorAll('.scroll-container')

  let isScrollingDown = false
  let btnRegister = document.getElementById('btnRegister')

  let projectFamilyRegion = [] // for setting active status in the menu
  let projectFamilyMenuItems = null

  let navDynamicContent = document.getElementById('navDynamicContent')

  let scrollBar = Scrollbar.init(scrollContainer, {
    // damping: 0.2,
  });

  for (let sc of otherScrollContainers) {
    Scrollbar.init(sc, {});
  }

  scrollBar.track.xAxis.element.remove()

  window.scrollBar = scrollBar
  let scenes = []
  let aosLIst = []
  let enableEffect = !isMobile
  let isHomePage = u.hasClass(document.body, 'page-home')
  let isFamilyPage = u.hasClass(document.body, 'page-family')
  let isFacilitiesPage = u.hasClass(document.body, 'page-facility')
  let btnChat = document.getElementById('btnChat')
  let sideButtons = document.getElementById('sideButtons')
  let footer = document.getElementById('footer')


  let aosGradientReveal = document.querySelectorAll(
    '.section-effect h2,' +
    '.section-effect h3,' +
    '.section-effect h4,' +
    '.section-effect h5,' +
    '.section-effect .btn,' +
    '.section-effect p')

  if (enableEffect) {
    if (isHomePage) {
      let homeHeroLogo = document.getElementById('homeHeroLogo')
      let homeHeroWrap = document.getElementById('homeHeroWrap')
      let homeDeco = document.getElementById('homeDeco')
      let heroTarget = document.querySelector('#homeHeroSlide .owl-stage-outer')
      homeDeco.style.marginBottom = '50vh' // should update this value if duration changed

      function renderHomeHero (elem, dy, percent) {
        homeHeroWrap.style.transform = 'translate3d(0,' + dy + 'px, 0)'
        homeDeco.style.transform = 'translate3d(0,' + dy + 'px, 0)'
      }

      function renderHomeHeroContent (elem, dy, percent) {
        let scale = 1 + percent * .5
        // elem.style.transform = 'translate3d(0,' + dy * -.7 + 'px, 0) scale3d(' + scale + ',' + scale + ',1)' // with translation
        elem.style.transform = 'scale3d(' + scale + ',' + scale + ',1)' // no translation
      }

      scenes.push(new Scene({
        elem: document.getElementById('homeHeroWrap'),
        triggerElem: document.getElementById('home-hero'),
        render: renderHomeHero,
        duration: .5,
        triggerOffset: 1,
      }))

      scenes.push(new Scene({
        elem: heroTarget,
        triggerElem: document.getElementById('home-hero'),
        render: renderHomeHeroContent,
        duration: 1.5,
        triggerOffset: 1,
      }))
    }

    // Common
    // ----------------------------------------
    for (let item of aosGradientReveal) {
      u.addClass(item, 'gradient-reveal')
      aosLIst.push(new Aos({
        elem: item,
        triggerOffset: .15,
      }))
    }
  }

  function callbackProjects (elem, isActive) {
    if (isActive) {
      u.addClass(navDynamicContent, 'active')
    } else {
      navDynamicContent.removeAttribute('style')
      u.removeClass(navDynamicContent, 'active')
    }
  }

  function callbackFacHero(elem, isActive) {
    if (isActive) {
      u.addClass(elem, 'has-swipe')
    }
  }

  // Always
  // ----------------------------------------

  // Home : Init
  // ----------------------------------------
  if (isHomePage) {
    u.addClass(navDynamicContent, 'enabled')
    scenes.push(new Aos({
      elem: document.getElementById('home-portfolio'),
      callback: callbackProjects,
      triggerOffset: 1,
      useSameTrigger: true,
    }))
  }

  // Family : Init
  // ----------------------------------------
  else if (isFamilyPage) {
    u.addClass(navDynamicContent, 'enabled')
    projectFamilyMenuItems = navDynamicContent.querySelectorAll('.project-select-item')
    scenes.push(new Aos({
      elem: document.getElementById('family-hero'),
      callback: callbackProjects,
      triggerOffset: 1.5,
      useSameTrigger: true,
    }))
  }

  // Facilities : Hero
  // ----------------------------------------
  else if (isFacilitiesPage) {
    scenes.push(new Aos({
      elem: document.getElementById('fac-hero'),
      callback: callbackFacHero,
      triggerOffset: 2,
      useSameTrigger: true,
    }))
  }

  // Slide : Autoplay
  // ----------------------------------------
  // let slides = document.querySelectorAll('.slide')
  let $slide = $('.slide')
  $slide.each(function () {
    let me = $(this)
    scenes.push(new Aos({
      elem: this,
      callback: function (elem, isActive) {
        if(isActive) {
          me.trigger('play.owl.autoplay', [(php_vars && php_vars.autoplayTimeout) || 6000]);
        }
      },
      triggerOffset: .2,
      useSameTrigger: true,
    }))
  })


  // Side Buttons (Chat & BackToTop)
  // ----------------------------------------
  let chatChangeTop = 0
  let chatChangeEl = null
  if (u.hasClass(document.body, 'page-home')) {
    chatChangeEl = document.getElementById('home-community')
  } else if (u.hasClass(document.body, 'page-about')) {
    chatChangeEl = document.getElementById('about-ceo')
  } else if (u.hasClass(document.body, 'page-facility')) {
    chatChangeEl = document.getElementById('fac-medical')
  } else if (u.hasClass(document.body, 'page-blog')) {
    chatChangeEl = document.getElementById('blog-story')
  }

  function renderSideButtons (elem, dy, percent) {
    if (percent === 0) {
      elem.removeAttribute('style')
    } else {
      if (window.innerWidth < u.breakpoints.sm) {
        elem.style.bottom = dy
      } else {
        elem.style.bottom = dy + 10
      }
    }
  }

  scenes.push(new Scene({
    elem: sideButtons,
    triggerElem: footer,
    render: renderSideButtons,
    triggerOffset: window.innerWidth < u.breakpoints.sm ? -.1 : 0,
    // debug: true,
  }))

  // Back to top
  // ----------------------------------------
  function callbackBackToTop (elem, isActive) {
    if (isActive) {
      u.addClass(elem, 'active')
    } else {
      u.removeClass(elem, 'active')
    }
  }

  aosLIst.push(new Aos({
    elem: document.getElementById('backToTop'),
    triggerElem: document.querySelector('#scrollContainer .scroll-content>*:first-child'),
    callback: callbackBackToTop,
    triggerOffset: 1.75,
    useSameTrigger: true,
  }))

  let onWindowResize = u.debounce(function () {
    debugScrollbar && console.log('[window resize]');
    let h = window.innerHeight
    let w = window.innerWidth

    if (enableEffect) {
      if (isHomePage) {
        let heroTarget = document.querySelector('#homeHeroSlide .owl-stage-outer')
        // heroTarget.style.height = '170%' // with translation
      }
    }

    // Scroller
    for (let item of scenes) {
      item.init(window.innerHeight)
    }
    for (let item of aosLIst) {
      item.init(window.innerHeight)
    }

    // Chat
    if (chatChangeEl) {
      chatChangeTop = u.offset(chatChangeEl).top
    }

    // Trigger scrollbar
    scrollBar.scrollTo(0, scrollBar.scrollTop + 1)
    scrollBar.scrollTo(0, scrollBar.scrollTop - 1)

    // Family : Project Region
    let sections = document.querySelectorAll('.project-item')
    projectFamilyRegion = []
    for (let sec of sections) {
      projectFamilyRegion.push(u.offset(sec).top + (window.innerHeight * .5))
    }

  }, 50);
  window.addEventListener("resize", onWindowResize)


  // Scrolling up / down
  // ----------------------------------------
  let setScrollingDown = u.debounce(function () {
    if (isScrollingDown) {
      u.addClass(btnRegister, 'hidden')
    }
  }, 50)

  let setScrollingUp = u.debounce(function () {
    if (!isScrollingDown) {
      u.removeClass(btnRegister, 'hidden')
    }
  }, 150)


  let lastLimitY = 0
  let lastOffsetY = 0
  scrollBar.addListener((status) => {

    if (lastLimitY !== status.limit.y) {
      lastLimitY = status.limit.y
      window.dispatchEvent(new Event('resize'))
    }

    if (lastOffsetY !== status.offset.y) {
      isScrollingDown = lastOffsetY < status.offset.y
      if (isScrollingDown) {
        setScrollingDown()
      } else {
        setScrollingUp()
      }
      lastOffsetY = status.offset.y
    }

    debugScrollbar && console.log('[scrollbar scroll]');

    let tOffset = status.offset.y
    let bOffset = tOffset + window.innerHeight

    for (let item of scenes) {
      item.render(bOffset)
    }
    for (let item of aosLIst) {
      item.render(bOffset)
    }

    // Chat
    if (bOffset <= chatChangeTop + 10) {
      btnChat.style.color = '#ffffff'
    } else {
      btnChat.removeAttribute('style')
    }

    // Family : Project Region
    if (isFamilyPage && projectFamilyRegion.length) {
      if (bOffset < projectFamilyRegion[0]) {

      } else {
        for (let i = projectFamilyRegion.length - 1; i >= 0; i--) {
          if (bOffset > projectFamilyRegion[i]) {
            if (!u.hasClass(projectFamilyMenuItems[i], 'active')) {
              for (let item of projectFamilyMenuItems) {
                u.removeClass(item, 'active')
              }
              u.addClass(projectFamilyMenuItems[i], 'active')
            }
            break
          }
        }
      }
    }

  })
}

class Aos {
  constructor (opt) {
    this.elem = opt.elem
    this.triggerElem = opt.triggerElem || opt.elem
    this.opt = Object.assign({}, {
      triggerOffset: 0,
    }, opt)
    this.isActive = false
    this.offset = {
      top: 0,
      left: 0,
    }
    this.offsetTrigger = {
      top: 0,
      left: 0,
    }
  }

  init (innerHeight) {
    this.isActive = false
    this.offset = u.offset(this.elem)
    this.offsetTrigger = u.offset(this.triggerElem)
    this.triggerStart = this.offsetTrigger.top + (this.opt.triggerOffset * innerHeight)
    this.triggerLeave = this.opt.useSameTrigger ? this.triggerStart : this.offset.top
    if (this.triggerStart < this.triggerLeave) {
      this.triggerLeave = this.triggerStart
    }
    if (this.opt.debug) {
      console.log('this.triggerStart', this.triggerStart);
    }
  }

  render (dy) {
    if (this.isActive) {
      if (dy < this.triggerLeave) {
        this.isActive = false
        this.opt.callback && this.opt.callback(this.elem, this.isActive)
        u.removeClass(this.elem, 'aos')
      }
    } else {
      if (dy >= this.triggerStart) {
        this.isActive = true
        this.opt.callback && this.opt.callback(this.elem, this.isActive)
        u.addClass(this.elem, 'aos')
      }
    }
  }
}

class Scene {
  constructor (opt) {
    this.elem = opt.elem
    this.triggerElem = opt.triggerElem || opt.elem
    this.opt = Object.assign({}, {
      triggerOffset: 0,
      duration: 1,
    }, opt)
    this.offset = {
      top: 0,
      left: 0,
    }
    this.offsetTrigger = {
      top: 0,
      left: 0,
    }
  }

  init (innerHeight) {
    this.offset = u.offset(this.elem)
    this.offsetTrigger = u.offset(this.triggerElem)
    this.triggerStart = this.offsetTrigger.top + (this.opt.triggerOffset * innerHeight)
    this.triggerEnd = this.triggerStart + (this.opt.duration * innerHeight)
    this.interval = this.triggerEnd - this.triggerStart
    this.leaved = false
    this.leavedBottom = false
    this.entered = false
    this.enteredBottom = false
  }

  render (dy) {
    if (dy > this.triggerEnd) {
      if (!this.leavedBottom) {
        this.opt.render && this.opt.render(this.elem, this.interval, 1)
        this.leavedBottom = true
      }
    } else if (dy < this.triggerStart) {
      if (!this.leaved) {
        this.opt.render && this.opt.render(this.elem, 0, 0)
        this.leaved = true
      }
    } else if (this.triggerStart <= dy) {
      this.leavedBottom = false
      this.leaved = false
      this.opt.render && this.opt.render(this.elem, dy - this.triggerStart, (dy - this.triggerStart) / this.interval)
    }
    // todo - triggerEnd
  }
}
