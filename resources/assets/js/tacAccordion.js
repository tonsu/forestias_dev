
export default function () {
  $('.tac-accordion').each(function () {
    let me = $(this)
    let inner = $('<div class="acc-inner"></div>')
    let itemWrap = $('<div class="acc-item-wrap"></div>')
    let sideWrap = $('<div class="acc-side-wrap"></div>')
    let items = me.children()
    itemWrap.append(items)
    inner.append(sideWrap)
    inner.append(itemWrap)
    me.append(inner)
    items.each(function () {
      let item = $(this)
      item.find('.head').append()
      let side = item.find('.acc-side')
      let content = item.find('.acc-content')
      content.wrap('<div class="acc-content-wrap"></div>')
      let contentWrap = item.find('.acc-content-wrap')
      let sideClone = side.clone()
      sideWrap.append(sideClone)
      contentWrap.css({
        height: 0,
      })
      // Click
      item.click(function () {
        if($(this).hasClass('active')) return

        $(this).siblings('.active').find('.acc-content-wrap').animate({
          height: 0 + 'px',
        }, 500, 'easeOutQuart')
        contentWrap.animate({
          height: content.outerHeight() + 'px',
        }, 500, 'easeOutQuart')

        // Ready to switch class
        $(this).addClass('active').siblings().removeClass('active')
        sideClone.addClass('active').siblings().removeClass('active')
      })
    })
    items.first().click()
  })
}
